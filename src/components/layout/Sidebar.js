import React, { Component } from 'react'
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

class Sidebar extends Component {

    state = {
        toggle: false
    }


    toggleMenuOpen = () => {
        this.setState({ toggle: !this.state.toggle });
    }
    render() {
        const { first_name, last_name, image_link } = this.props.userData
        return (
            <div>
                <aside className="main-sidebar sidebar-dark-primary elevation-4">
                    {/* Brand Logo */}
                    <a href="index3.html" className="brand-link navbar-danger">
                        <img src="/dist/img/elegent_tex_logo.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
                        <span className="brand-text font-weight-light"><b>Elegent</b>Tex</span>
                    </a>
                    {/* Sidebar */}
                    <div className="sidebar">
                        {/* Sidebar user panel (optional) */}
                        <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                            <div className="image">
                                {image_link ? <img src={process.env.REACT_APP_STORAGE + image_link} className="img-circle elevation-2" alt="User" /> :
                                    <img src='/images/avatar.png' className="img-circle elevation-2" alt="User" />}
                            </div>
                            <div className="info">
                                <NavLink to="/profile" className="d-block">{first_name} {last_name}</NavLink>
                            </div>
                        </div>
                        {/* Sidebar Menu */}
                        <nav className="mt-2">
                            <ul className="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                                {/* Add icons to the links using the .nav-icon class with font-awesome or any other icon font library */}
                                {/* <li className="nav-item">
                                    <NavLink to="/home" className="nav-link" activeclassname="active">
                                        <i className="nav-icon fas fa-calendar-alt" />
                                        <p>Home<span className="badge badge-info right">2</span></p>
                                    </NavLink>
                                </li> */}
                                <li className="nav-item">
                                    <NavLink to="/" exact className="nav-link" activeclassname="active">
                                        <i className="nav-icon fas fa-tachometer-alt" />
                                        <p>Dashboard</p>
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/orders" exact className="nav-link" activeclassname="active">
                                        <i className="nav-icon fas fa-tachometer-alt" />
                                        <p>Orders</p>
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/products" exact className="nav-link" activeclassname="active">
                                        <i className="nav-icon fa fa-shopping-bag" />
                                        <p>Products</p>
                                    </NavLink>
                                </li>
                                {/* <li className={this.state.toggle ? "nav-item has-treeview" : "nav-item has-treeview menu-open"}>
                                    <div className="nav-link" activeclassname="active" onClick={this.toggleMenuOpen}>
                                        <i className="nav-icon fas fa-tachometer-alt" />
                                        <p>
                                            Houses
                                        <i className="right fas fa-angle-left" />
                                        </p>
                                    </div>
                                    <ul className="nav nav-treeview">
                                        <li className="nav-item">
                                            <NavLink to="/dashboard" className="nav-link" activeclassname="active">
                                                <i className="far fa-circle nav-icon" />
                                                <p>Dashboard v1</p>
                                            </NavLink>
                                        </li>
                                    </ul>
                                </li> */}

                                <li className="nav-header">EXTRAS</li>
                                <li className="nav-item">
                                    <NavLink to="/settings" exact className="nav-link" activeclassname="active">
                                        <i className="nav-icon fa fa-cog" />
                                        <p>Settings</p>
                                    </NavLink>
                                </li>
                            </ul>
                        </nav>
                        {/* /.sidebar-menu */}
                    </div>
                    {/* /.sidebar */}
                </aside>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        userData: state.auth.user
    }
}

export default connect(mapStateToProps)(Sidebar);
import React, { Component } from 'react'
import axios from 'axios'
import { formValid } from '../../services/FormValidationService';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { returnErrors, clearErrors } from '../../store/actions/errorActions';
import { connect } from 'react-redux';
import Input from '../helpers/input';


toast.configure();
const phoneRegex = RegExp(
    /^[0][1-9]\d{9}$|^[1-9]\d{11}$/
);
// const nidRegex = RegExp(
//     /^\d{7}(\d{6})?$/
// );
class RegisterHouse extends Component {

    state = {
        regHouseForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter House Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            houseNo: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter House Number'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            floorCount: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Number of floors'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            streetNo: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Street Number'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Phone Number'
                },
                value: '',
                validation: {
                    required: true,
                    isPhone: true,
                },
                valid: false,
                touched: false
            },

            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Street Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            wardNo: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Ward or Union Number'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            division: {
                elementType: 'select',
                elementConfig: {
                    options: [],
                    placeholder: 'Select Your Division'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            district: {
                elementType: 'select',
                elementConfig: {
                    options: [],
                    placeholder: 'Select Your District'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            upazila: {
                elementType: 'select',
                elementConfig: {
                    options: [],
                    placeholder: 'Select Your Upazila'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
        },
        validationMsg: ''

    }

    checkFormValidity = (value, rules) => {
        let isValid = true;

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
            this.setState({ validationMsg: 'Please enter a valid input' })
        }
        if (rules.isPhone) {
            isValid = phoneRegex.test(value) && isValid;
            this.setState({ validationMsg: 'Please enter a valid phone number' })
        }
        return isValid;
    }

    handleChange = (event, inputIdentifier) => {

        const updatedRegForm = { ...this.state.regHouseForm };
        const updatedFormElement = { ...updatedRegForm[inputIdentifier] };
        console.log(updatedFormElement)
        if (updatedFormElement.elementType === 'select') {
            updatedFormElement.value = event;
            updatedRegForm[inputIdentifier] = updatedFormElement;
            updatedFormElement.touched = true;
            updatedFormElement.valid = this.checkFormValidity(updatedFormElement.value.label, updatedFormElement.validation);
            switch (inputIdentifier) {
                case 'division':
                    this.setState({ regHouseForm: updatedRegForm }, () => this.getSelectedDistricts());
                    updatedRegForm['district'].value = null;
                    updatedRegForm['upazila'].value = null;
                    break;
                case 'district':
                    this.setState({ regHouseForm: updatedRegForm }, () => this.getSelectedUpazilas());
                    console.log(updatedRegForm['upazila'])
                    break
                case 'upazila':
                    this.setState({ regHouseForm: updatedRegForm });
                    break
                default:
                    break;
            }
        } else {
            updatedFormElement.value = event.target.value;
            updatedFormElement.valid = this.checkFormValidity(updatedFormElement.value, updatedFormElement.validation);
            updatedFormElement.touched = true;
            console.log(updatedFormElement);
            updatedRegForm[inputIdentifier] = updatedFormElement;
            this.setState({ regHouseForm: updatedRegForm });
            console.log(this.state.validationMsg)
        }

    }

    registerHouse = (event) => {
        event.preventDefault();
        const formData = {};

        for (let formElementIdentifier in this.state.regHouseForm) {
            console.log(formElementIdentifier)
            formData[formElementIdentifier] = this.state.regHouseForm[formElementIdentifier].value;
        }
        console.log(formData);
        // if (formValid(this.state)) {
        //     axios.post(process.env.REACT_APP_API_KEY+'/api/registerHouse', houseData).then(response => {
        //         console.log(response.data);
        //         toast.success("House Registered Successfully !!!");
        //         window.$("#reg-house-modal").modal("hide");
        //     }).catch(err => {
        //         this.props.returnErrors(err.response.data.message, err.response.status, 'REGISTER_FAIL');
        //         console.log(err)
        //     })
        // }
        // else {
        //     toast.error("Please fill all the required fields !!!");
        // }
        // if (this.state.errorMsg) {
        //     setTimeout(function () { this.props.clearErrors() }, 3000);
        // }
        // console.log(this.state)
    }


    getAllDivisions = () => {
        axios.get(process.env.REACT_APP_API_KEY + '/api/getAllDivisions').then(response => {
            let data = this.structureSelectOptions(response.data);
            const updatedRegForm = { ...this.state.regHouseForm }
            const updatedDivision = { ...updatedRegForm.division }
            const updatedElementConfig = { ...updatedDivision.elementConfig, options: data }
            updatedDivision['elementConfig'] = updatedElementConfig
            updatedRegForm['division'] = updatedDivision;
            this.setState({ regHouseForm: updatedRegForm });
        }).catch(err => {
            console.log(err);
        })
    }

    getSelectedDistricts = () => {
        const selectedDivision = this.state.regHouseForm.division.value.value
        axios.post(`http://127.0.0.1:8000/api/getSelectedDistricts?division_id=${selectedDivision}`).then(response => {
            let data = this.structureSelectOptions(response.data);
            const updatedRegForm = { ...this.state.regHouseForm }
            const updatedDistrict = { ...updatedRegForm.district }
            const updatedElementConfig = { ...updatedDistrict.elementConfig, options: data }
            updatedDistrict['elementConfig'] = updatedElementConfig
            updatedRegForm['district'] = updatedDistrict;
            this.setState({ regHouseForm: updatedRegForm });
        }).catch(err => {
            console.log(err);
        });
    }
    getSelectedUpazilas = () => {
        const selectedDistrict = this.state.regHouseForm.district.value.value
        axios.post(`http://127.0.0.1:8000/api/getSelectedUpazilas?district_id=${selectedDistrict}`).then(response => {
            let data = this.structureSelectOptions(response.data);
            const updatedRegForm = { ...this.state.regHouseForm }
            const updatedUpazila = { ...updatedRegForm.upazila }
            const updatedElementConfig = { ...updatedUpazila.elementConfig, options: data }
            updatedUpazila['elementConfig'] = updatedElementConfig
            updatedRegForm['upazila'] = updatedUpazila;
            this.setState({ regHouseForm: updatedRegForm });
        }).catch(err => {
            console.log(err);
        });
    }

    structureSelectOptions = (data) => {
        let options = [];
        data.forEach(function (element) {
            let temp_division = {
                value: element.id,
                label: element.name
            }
            options.push(temp_division)
        });
        return options;
    }

    componentDidMount() {
        this.getAllDivisions();
        console.log("Register House Mounted");
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.regHouseForm) {
            formElementsArray.push({
                id: key,
                config: this.state.regHouseForm[key]
            });
        }
        let form = (
            <form onSubmit={this.registerHouse}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {
                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                validationMsg={this.state.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" className="btn btn-primary">Save changes</button>
                </div>
            </form>
        );
        return (
            <div className="modal fade" id="reg-house-modal">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">House Registration</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>

                        {form}


                    </div>
                    {/* /.modal-content */}
                </div>
                {/* /.modal-dialog */}
            </div>

        )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    userData: state.auth.user,
    error: state.error
})

export default connect(mapStateToProps, { returnErrors, clearErrors })(RegisterHouse);
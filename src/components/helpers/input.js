import React from 'react';
import Select from 'react-select';
import Datepicker from 'react-datepicker';
import '../../App.css'

export default function input(props) {


    let inputElement = null;
    switch (props.elementType) {
        case 'input':
            inputElement = <input {...props.elementConfig} value={props.value} className={props.validationMsg && props.touched ? 'form-control is-invalid' : 'form-control'} onChange={props.changeHandler} name={props.elementName} />
            break;
        case 'file':
            inputElement = <input {...props.elementConfig} className="form-control-file" onChange={props.changeHandler} name={props.elementName} key={props.imageKey} />
            break;
        case 'multiFile':
            inputElement = <input {...props.elementConfig} multiple className="form-control-file" onChange={props.changeHandler} name={props.elementName} key={props.imageKey} />
            break;
        case 'select':
            inputElement = <Select {...props.elementConfig} value={props.value} onChange={props.changeHandler} name={props.elementName} />
            break;
        case 'date':
            inputElement = <Datepicker {...props.elementConfig} selected={props.value} className='form-control' onChange={props.changeHandler} />
            break;
        case 'textarea':
            inputElement = <textarea {...props.elementConfig} className="form-control" value={props.value} onChange={props.changeHandler}  ></textarea>
            break;

        default:
            inputElement = <input {...props.elementConfig} selected={props.value} className={props.invalid && props.touched ? 'form-control' : 'form-control is-invalid'} onChange={props.changeHandler} name={props.elementName} />
            break;
    }
    return (
        <div className={"col-sm-12 " + props.width}>
            <div className="form-group">
                <label htmlFor={props.elementName}>{props.label} :</label>
                {inputElement}
                {props.validationMsg && props.touched ? <div className="text-danger">
                    <small>{props.validationMsg}</small>
                </div> : null}
            </div>
        </div>
    )
}

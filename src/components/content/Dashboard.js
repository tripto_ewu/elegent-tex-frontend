import React, { Component } from 'react'
import RegisterHouse from '../houses/RegisterHouse'
import { connect } from 'react-redux';
import ContentHeader from '../extras/ContentHeader';

import { NavLink } from 'react-router-dom';

class Dashboard extends Component {

    componentDidMount() {
        window.Echo.channel('chat').listen('NewNotifications', (e) => {
            console.log(e)
        })

    }


    render() {
        return (
            <div className="content-wrapper">
                {/* Content Header (Page header) */}
                <ContentHeader name="Dashboard" />
                {/* Main content */}
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6">
                                {/* USERS LIST */}
                                <div className="card">
                                    <div className="card-header border-transparent">
                                        <h3 className="card-title">Latest Orders</h3>
                                        <div className="card-tools">
                                            <button type="button" className="btn btn-tool" data-card-widget="collapse">
                                                <i className="fas fa-minus" />
                                            </button>
                                            <button type="button" className="btn btn-tool" data-card-widget="remove">
                                                <i className="fas fa-times" />
                                            </button>
                                        </div>
                                    </div>
                                    {/* /.card-header */}
                                    <div className="card-body p-0">
                                        <div className="table-responsive">
                                            <table className="table m-0">
                                                <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>Item</th>
                                                        <th>Status</th>
                                                        <th>Popularity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                                        <td>Call of Duty IV</td>
                                                        <td><span className="badge badge-success">Shipped</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#00a65a" data-height={20}>90,80,90,-70,61,-83,63</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                                        <td>Samsung Smart TV</td>
                                                        <td><span className="badge badge-warning">Pending</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#f39c12" data-height={20}>90,80,-90,70,61,-83,68</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                                        <td>iPhone 6 Plus</td>
                                                        <td><span className="badge badge-danger">Delivered</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#f56954" data-height={20}>90,-80,90,70,-61,83,63</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                                        <td>Samsung Smart TV</td>
                                                        <td><span className="badge badge-info">Processing</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#00c0ef" data-height={20}>90,80,-90,70,-61,83,63</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                                        <td>Samsung Smart TV</td>
                                                        <td><span className="badge badge-warning">Pending</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#f39c12" data-height={20}>90,80,-90,70,61,-83,68</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                                        <td>iPhone 6 Plus</td>
                                                        <td><span className="badge badge-danger">Delivered</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#f56954" data-height={20}>90,-80,90,70,-61,83,63</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                                        <td>Call of Duty IV</td>
                                                        <td><span className="badge badge-success">Shipped</span></td>
                                                        <td>
                                                            <div className="sparkbar" data-color="#00a65a" data-height={20}>90,80,90,-70,61,-83,63</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        {/* /.table-responsive */}
                                    </div>
                                    {/* /.card-body */}
                                    <div className="card-footer clearfix">
                                        {/* <a href="!#" className="btn btn-sm btn-info float-left">Place New Order</a> */}
                                        <NavLink to="/createNewOrder" exact activeclassname="active" className="btn btn-sm btn-info float-left" >
                                            Place New Order
                                        </NavLink>
                                        <a href="!#" className="btn btn-sm btn-secondary float-right">View All Orders</a>
                                    </div>
                                    {/* /.card-footer */}
                                </div>

                                {/*/.card */}
                            </div>

                        </div>
                    </div>

                </section>
                {/* /.content */}
            </div>

        )
    }
}

const mapStateToProps = state => ({
    userData: state.auth.user,
    token: state.auth.access_token,
})

export default connect(mapStateToProps)(Dashboard);

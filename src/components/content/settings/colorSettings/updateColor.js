import React, { Component } from 'react';
import Input from '../../../helpers/input'
import { toast } from 'react-toastify';
import axios from 'axios';
import { formValid } from '../../../helpers/formValidationChecker';
import { connect } from 'react-redux';
import { updateExistingColors } from '../../../../store/actions/productActions';

toast.configure();
class UpdateColor extends Component {

    state = {
        colorForm: {
            colorName: {
                elementType: 'input',
                label: 'Color name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter color name'
                },
                value: this.props.selectedColor.color_name,
                validationMsg: '',
                touched: false
            },
            colorHash: {
                elementType: 'input',
                label: 'Color hash code',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter color hash code'
                },
                value: this.props.selectedColor.color_hash,
                validationMsg: '',
                touched: false
            },
            isAvailable: {
                elementType: 'select',
                label: 'Color availability',
                elementConfig: {
                    options: [{ value: 0, label: 'Unavailable' }, { value: 1, label: 'Available' }],
                    placeholder: 'Select color availability'
                },
                value: { value: this.props.selectedColor.is_available, label: this.props.selectedColor.is_available == 0 ? 'Unavailable' : 'Available' },
                validationMsg: '',
                touched: false
            },
        },
        formIsvalid: false,
        validationMsg: ''
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'colorName':
                validationMsg = value.length < 1 ? "Enter Color Name" : '';
                return validationMsg;
            case 'colorHash':
                validationMsg = value.length < 1 ? "Enter Color Hash Code" : '';
                return validationMsg;

            default:
                break;
        }

    }

    handleChange = (event, inputIdentifier) => {
        const updatedColorForm = { ...this.state.colorForm };
        const updatedFormElement = { ...updatedColorForm[inputIdentifier] };
        if (updatedFormElement.elementType === 'select') {
            updatedFormElement.value = event;
            updatedFormElement.valid = this.checkFormValidity(updatedFormElement.value.label, inputIdentifier);
        } else {
            updatedFormElement.value = event.target.value;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        }
        updatedFormElement.touched = true;

        updatedColorForm[inputIdentifier] = updatedFormElement;
        let formIsvalid = true;
        for (let inputIdentifier in updatedColorForm) {
            formIsvalid = updatedColorForm[inputIdentifier].valid && formIsvalid;
        }
        this.setState({ colorForm: updatedColorForm, formIsvalid: formIsvalid });

    }
    updateColor = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {}
        for (let formElementIdentifier in this.state.colorForm) {
            if (this.state.colorForm[formElementIdentifier].elementType === 'select') {
                formData[formElementIdentifier] = this.state.colorForm[formElementIdentifier].value.value;
            } else {
                formData[formElementIdentifier] = this.state.colorForm[formElementIdentifier].value
            }
            formError[formElementIdentifier] = this.state.colorForm[formElementIdentifier].validationMsg;
        }
        const id = this.props.selectedColor.id;
        console.log(formData);
        console.log(formError);
        if (formValid(formError, formData)) {
            axios.post(`${process.env.REACT_APP_API_KEY}/api/updateProductColor/${id}`, formData).then(response => {
                this.props.updateExistingColors(response.data, 'update');
                toast.success("Color is updated !!!");
                window.$("#update-color-modal").modal("hide");
                this.props.unmountUpdateColor();
            }).catch(err => {

                toast.success("Color Update Error!!!");
            })
        }
        else {
            toast.error("Please fill all the required fields !!!");
        }

    }

    componentDidMount() {
        window.$('#update-color-modal').modal('show');
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.colorForm) {
            formElementsArray.push({
                id: key,
                config: this.state.colorForm[key]
            });
        }
        let form = (
            <form onSubmit={this.updateColor}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {

                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                label={formElement.config.label}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                validationMsg={formElement.config.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.props.unmountUpdateColor}>Close</button>
                    <button type="submit" className="btn btn-primary">Save changes</button>
                </div>
            </form>
        );
        return (
            <div className="modal fade" data-keyboard="false" data-backdrop="static" id="update-color-modal">
                <div className="modal-dialog modal-md">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Edit Color</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.unmountUpdateColor}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {form}
                    </div>
                </div>
            </div>

        )
    }
}
export default connect(null, { updateExistingColors })(UpdateColor);
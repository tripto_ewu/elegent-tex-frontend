import React, { Component } from 'react';
import Input from '../../../helpers/input'
import { toast } from 'react-toastify';
import axios from 'axios';
import { connect } from 'react-redux';
import { updateExistingColors } from '../../../../store/actions/productActions';
import { formValid } from '../../../helpers/formValidationChecker'

toast.configure();
class RegisterColor extends Component {

    state = {
        addColorForm: {
            colorName: {
                elementType: 'input',
                label: 'Color name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter color name'
                },
                value: '',
                validationMsg: '',
                width: 'col-md-12',
                touched: false
            },
            colorHash: {
                elementType: 'input',
                label: 'Color hash code',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter color hash code'
                },
                value: '',
                validationMsg: '',
                touched: false,
                width: 'col-md-12',
            },
        },
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'colorName':
                validationMsg = value.length < 1 ? "Enter color name" : '';
                return validationMsg;
            case 'colorHash':
                validationMsg = value.length < 1 ? "Enter color hashcode" : '';
                return validationMsg;
            default:
                return validationMsg;
        }

    }

    handleChange = (event, inputIdentifier) => {

        const updatedAddColorForm = { ...this.state.addColorForm };
        const updatedFormElement = { ...updatedAddColorForm[inputIdentifier] };
        updatedFormElement.value = event.target.value;
        updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        updatedFormElement.touched = true;
        updatedAddColorForm[inputIdentifier] = updatedFormElement;
        this.setState({ addColorForm: updatedAddColorForm });
    }
    registerColor = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {};

        for (let formElementIdentifier in this.state.addColorForm) {
            formData[formElementIdentifier] = this.state.addColorForm[formElementIdentifier].value;
            formError[formElementIdentifier] = this.state.addColorForm[formElementIdentifier].validationMsg;
        }
        if (formValid(formError, formData)) {
            axios.post(process.env.REACT_APP_API_KEY + '/api/addProductColor', formData).then(response => {
                this.props.updateExistingColors(response.data, 'add');
                toast.success("Color Registered Successfully !!!");
                window.$("#add-color-modal").modal("hide");
                this.props.unmountUpdateColor();
            }).catch(err => {
                //this.props.returnErrors(err.response.data.message, err.response.status, 'REGISTER_FAIL');
                console.log(err)
            })
        }
        else {
            toast.error("Please fill all the required fields !!!");
        }
        if (this.state.errorMsg) {
            setTimeout(function () { this.props.clearErrors() }, 3000);
        }
    }
    componentDidMount() {
        window.$('#add-color-modal').modal('show');
    }
    componentWillUnmount() {
        window.$('#add-color-modal').modal('hide');
    }


    render() {
        const formElementsArray = [];
        for (let key in this.state.addColorForm) {
            formElementsArray.push({
                id: key,
                config: this.state.addColorForm[key]
            });
        }
        let form = (
            <form onSubmit={this.registerColor}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {
                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                label={formElement.config.label}
                                value={formElement.config.value}
                                validationMsg={formElement.config.validationMsg}
                                touched={formElement.config.touched}
                                width={formElement.config.width}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default" onClick={this.props.unmount()}>Close</button>
                    <button type="submit" className="btn btn-primary">Save changes</button>
                </div>
            </form>
        );
        return (
            <div className="modal fade" id="add-color-modal">
                <div className="modal-dialog modal-md">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Add New Color</h4>
                            <button type="button" className="close" onClick={this.props.unmount()}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {form}
                    </div>
                </div>
            </div>

        )
    }
}


export default connect(null, { updateExistingColors })(RegisterColor);
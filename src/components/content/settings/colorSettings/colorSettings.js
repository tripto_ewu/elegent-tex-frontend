import React, { Component } from 'react';
import RegisterColor from './RegisterColor';
import UpdateColor from './updateColor';
import axios from 'axios';
import WarningModal from '../../../extras/WarningModal';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { loadProductColor, updateExistingColors } from '../../../../store/actions/productActions';

toast.configure();
class colorSettings extends Component {

    state = {
        productColors: [],
        selectedColor: '',
        unmountUpdateColor: true,
        unmountAddColor: true,
        unmountWarningModal: true,
    }

    getAllColors = () => {
        axios.get(process.env.REACT_APP_API_KEY + '/api/getAllColors').then(response => {
            this.setState({ productColors: response.data });
        }).catch(error => {
            console.log(error);
        })
    }
    deleteColor = (color) => {
        this.handleWarningModal()
        this.setState({ selectedColor: color });

    }
    confirmDeleteColor = () => {
        const colorId = this.state.selectedColor.id
        axios.get(`${process.env.REACT_APP_API_KEY}/api/deleteProductColor/${colorId}`).then(response => {
            // this.props.updateColor(response.data);
            // toast.success("Color is updated !!!");
            window.$("#warning-modal").modal("hide");
            this.handleWarningModal();
            this.props.updateExistingColors(response.data, 'delete');
            console.log(response);
        }).catch(err => {

            toast.success("Color Update Error!!!");
        })
    }
    updateColorArray = (color, operation) => {
        console.log(color)
        console.log(operation)
        const updatedColors = [...this.state.productColors];
        var foundIndex = updatedColors.findIndex(x => x.id === color.id);
        if (foundIndex !== -1) {
            operation === 'update' ? updatedColors[foundIndex] = color : updatedColors.splice(foundIndex, 1);
        } else {
            updatedColors.unshift(color)
        }
        this.setState({ productColors: updatedColors, selectedColor: null });
    }

    updateColor = (color) => {
        this.setState({ selectedColor: color, unmountUpdateColor: !this.state.unmountUpdateColor })
    }
    handleUnmountUpdate = () => {
        this.setState({ unmountUpdateColor: !this.state.unmountUpdateColor })
    }
    handleUnmountAdd = () => {
        this.setState({ unmountAddColor: !this.state.unmountAddColor }, () => {
            console.log(this.state)
        })
    }
    handleWarningModal = () => {
        this.setState({ unmountWarningModal: !this.state.unmountWarningModal })
    }
    componentDidMount() {
        this.props.loadProductColor();

    }
    componentDidUpdate() {

    }
    render() {
        const productColors = this.props.productColors
        let colors = (productColors ? productColors.map((color) => {

            return <tr key={color.id}>
                <td><a href="pages/examples/invoice.html">OR9842</a></td>
                <td>{color.color_name}</td>
                <td className="project-state">
                    {color.is_available ? <span className="badge badge-success">Available</span> : <span className="badge badge-danger">Not Available</span>}
                </td>
                <td className="project-actions text-right ">
                    <button className="btn btn-info btn-sm mx-1"
                        type="button" onClick={() => this.updateColor(color)}>
                        <i className="fas fa-pencil-alt">
                        </i></button>
                    <button className="btn btn-danger btn-sm " data-toggle="modal"
                        data-target="#warning-modal" onClick={() => this.deleteColor(color)}>
                        <i className="fas fa-trash">
                        </i></button>
                </td>
            </tr>
        }) : <tr>Loading...</tr>)
        return (
            <div className="col-md-6">
                <div className="card">
                    <div className="card-header border-transparent bg-info">
                        <h2 className="card-title">Color Settings</h2>
                    </div>
                    {/* /.card-header */}
                    <div className="card-body p-0">
                        <div className="table-responsive">
                            <table className="table m-0">
                                <thead>
                                    <tr>
                                        <th>Color ID</th>
                                        <th>Color</th>
                                        <th>Availability</th>
                                        <th className="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {colors}
                                </tbody>
                            </table>
                        </div>
                        {/* /.table-responsive */}
                    </div>
                    {/* /.card-body */}
                    {!this.state.unmountAddColor ? <RegisterColor unmount={() => this.handleUnmountAdd} productColors={productColors} /> : null}
                    {!this.state.unmountUpdateColor ?
                        <UpdateColor updateColor={this.updateColorArray}
                            unmountUpdateColor={this.handleUnmountUpdate}
                            selectedColor={this.state.selectedColor} /> : null}
                    <div className="card-footer clearfix">
                        <button className="btn btn-sm btn-info float-left" onClick={this.handleUnmountAdd}>New Product Color</button>
                    </div>
                    {/* /.card-footer */}
                </div>

                {!this.state.unmountWarningModal ? <WarningModal
                    unmountModal={this.handleWarningModal}
                    message="Warning!!! Are You Sure?"
                    action={this.confirmDeleteColor}
                /> : null}

            </div>
        )
    }
}
const mapStateToProps = state => ({
    productColors: state.products.productColors,
    error: state.error
})
export default connect(mapStateToProps, { loadProductColor, updateExistingColors })(colorSettings);
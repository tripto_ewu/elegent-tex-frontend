import React, { Component } from 'react';
import ContentHeader from '../../extras/ContentHeader';
import ColorSettings from './colorSettings/colorSettings';
import ProductTypeSettings from './productTypeSettings/ProductTypeSettings'


export default class Settings extends Component {
    render() {
        return (
            <div className="content-wrapper">
                <ContentHeader name="Settings" />
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <ColorSettings />
                            <ProductTypeSettings></ProductTypeSettings>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

import React, { Component } from 'react'
import RegisterProductType from './RegisterProductType';
import UpdateProductType from './UpdateProductType';
import { connect } from 'react-redux';
import axios from 'axios';
import WarningModal from '../../../extras/WarningModal';
import { toast } from 'react-toastify';
import { loadProductTypes } from '../../../../store/actions/productActions';


toast.configure();

class ProductTypeSettings extends Component {

    state = {
        productTypes: [],
        selectedProductTypes: '',
        unmountUpdateProductType: true,
        unmountRegisterProductType: true,
        unmountWarningModal: true,
    }

    getAllProducts = () => {
        axios.get(process.env.REACT_APP_API_KEY + '/api/getAllProducts').then(response => {
            this.setState({ productTypes: response.data });
        }).catch(error => {
            console.log(error);
        })
    }
    deleteProduct = (product) => {
        this.handleWarningModal()
        this.setState({ selectedProductTypes: product });

    }
    confirmDeleteProduct = () => {
        const typeId = this.state.selectedProductTypes.id
        axios.get(`${process.env.REACT_APP_API_KEY}/api/deleteProducType/${typeId}`).then(response => {
            // this.props.updateColor(response.data);
            // toast.success("Color is updated !!!");
            window.$("#warning-modal").modal("hide");
            this.handleWarningModal();
            this.updateProductArray(response.data, 'delete')
            console.log(response);
        }).catch(err => {

            toast.success("Color Update Error!!!");
        })
    }
    updateProductArray = (product, operation) => {
        console.log(product)
        console.log(operation)
        const updatedProducts = [...this.state.productTypes];
        var foundIndex = updatedProducts.findIndex(x => x.id === product.id);
        if (foundIndex !== -1) {
            operation === 'update' ? updatedProducts[foundIndex] = product : updatedProducts.splice(foundIndex, 1);
        } else {
            updatedProducts.unshift(product)
        }
        this.setState({ productTypes: updatedProducts, selectedProductTypes: null });
    }

    updateProduct = (product) => {
        this.setState({ selectedProductTypes: product, unmountUpdateProductType: !this.state.unmountUpdateProductType })
    }
    handleUnmountUpdate = () => {
        this.setState({ unmountUpdateProductType: !this.state.unmountUpdateProductType })
    }
    handleWarningModal = () => {
        this.setState({ unmountWarningModal: !this.state.unmountWarningModal })
    }
    componentDidMount() {
        //this.getAllProducts();
        this.props.loadProductTypes();

    }
    componentDidUpdate() {
        console.log(this.props)
        console.log("hello")
    }


    render() {
        const productTypes = this.props.productTypes;
        let types = (productTypes ? productTypes.map((type) => {
            return <tr key={type.id}>
                <td><a href="!#">{`PR${type.id}`}</a></td>
                <td>{type.type_name}</td>
                <td className="project-state">
                    {type.is_active ? <span className="badge badge-success">Active</span> : <span className="badge badge-danger">Inactive</span>}
                </td>
                <td className="project-actions text-right ">
                    <button className="btn btn-info btn-sm mx-1" onClick={() => this.updateProduct(type)}>
                        <i className="fas fa-pencil-alt">
                        </i></button>
                    <button className="btn btn-danger btn-sm " data-toggle="modal"
                        data-target="#warning-modal" onClick={() => this.deleteProduct(type)}>
                        <i className="fas fa-trash">
                        </i></button>
                </td>
            </tr>
        }) : <tr>Loading...</tr>)
        return (
            <div className="col-md-6">
                <div className="card">
                    <div className="card-header border-transparent bg-info">
                        <h2 className="card-title">ProductTypes</h2>
                    </div>
                    {/* /.card-header */}
                    <div className="card-body p-0">
                        <div className="table-responsive">
                            <table className="table m-0">
                                <thead>
                                    <tr>
                                        <th>Type ID</th>
                                        <th>Product Type</th>
                                        <th>Active</th>
                                        <th className="text-right">Action</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    {types}
                                </tbody>
                            </table>
                        </div>
                        {/* /.table-responsive */}
                    </div>
                    {/* /.card-body */}
                    {!this.state.unmountRegisterProductType ?
                        <RegisterProductType
                            newProduct={this.updateProductArray}
                            unmountRegister={() => this.setState({ unmountRegisterProductType: !this.state.unmountRegisterProductType })} /> : null}
                    {!this.state.unmountUpdateProductType ?
                        <UpdateProductType updateProduct={this.updateProductArray}
                            unmountUpdateProductType={this.handleUnmountUpdate}
                            selectedProductTypes={this.state.selectedProductTypes} /> : null}
                    <div className="card-footer clearfix">
                        <button
                            className="btn btn-sm btn-info float-left"
                            onClick={(e) => this.setState({ unmountRegisterProductType: !this.state.unmountRegisterProductType })}>
                            New Product Type</button>
                    </div>
                    {/* /.card-footer */}
                </div>

                {!this.state.unmountWarningModal ? <WarningModal
                    unmountModal={this.handleWarningModal}
                    message="Warning!!! Are You Sure?"
                    action={this.confirmDeleteProduct}
                /> : null}

            </div>
        )
    }
}
const mapStateToProps = state => ({
    productTypes: state.products.productTypes
})
export default connect(mapStateToProps, { loadProductTypes })(ProductTypeSettings);
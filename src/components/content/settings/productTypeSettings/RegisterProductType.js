import React, { Component } from 'react';
import Input from '../../../helpers/input'
import { toast } from 'react-toastify';
import axios from 'axios';
import { connect } from 'react-redux';
import { updateExistingColors } from '../../../../store/actions/productActions';
import { formValid } from '../../../helpers/formValidationChecker'

toast.configure();
class RegisterProductType extends Component {

    state = {
        productTypeForm: {
            productType: {
                elementType: 'input',
                label: 'Product Type',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Please enter product type'
                },
                value: '',
                width: 'col-md-12',
                validationMsg: '',
                touched: false
            },
        },
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'productName':
                validationMsg = value.length < 1 ? "Enter Product Name" : '';
                return validationMsg;
            default:
                return validationMsg;
        }

    }

    handleChange = (event, inputIdentifier) => {

        const updatedproductTypeForm = { ...this.state.productTypeForm };
        const updatedFormElement = { ...updatedproductTypeForm[inputIdentifier] };
        console.log(updatedFormElement)

        updatedFormElement.value = event.target.value;
        updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value.label, inputIdentifier);
        updatedFormElement.touched = true;
        console.log(updatedFormElement);
        updatedproductTypeForm[inputIdentifier] = updatedFormElement;
        this.setState({ productTypeForm: updatedproductTypeForm });
    }
    registerProductType = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {};

        for (let formElementIdentifier in this.state.productTypeForm) {

            formData[formElementIdentifier] = this.state.productTypeForm[formElementIdentifier].value

            formError[formElementIdentifier] = this.state.productTypeForm[formElementIdentifier].validationMsg;
        }
        if (formValid(formError, formData)) {
            console.log(formData)
            axios.post(process.env.REACT_APP_API_KEY + '/api/addProductType', formData).then(response => {
                //this.props.updateExistingColors(response.data, 'add');
                console.log(response)
                toast.success("House Registered Successfully !!!");
                window.$("#add-type-modal").modal("hide");
            }).catch(err => {
                //this.props.returnErrors(err.response.data.message, err.response.status, 'REGISTER_FAIL');
                console.log(err)
            })
        }
        else {
            toast.error("Please fill all the required fields !!!");
        }
        if (this.state.errorMsg) {
            setTimeout(function () { this.props.clearErrors() }, 3000);
        }
        console.log(this.state)
    }

    componentDidMount() {
        window.$("#add-type-modal").modal("show");
    }
    componentWillUnmount() {
        window.$("#add-type-modal").modal("hide");
    }




    render() {
        const formElementsArray = [];
        for (let key in this.state.productTypeForm) {
            formElementsArray.push({
                id: key,
                config: this.state.productTypeForm[key]
            });
        }
        let form = (
            <form onSubmit={this.registerProductType}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {
                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                label={formElement.config.label}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                width={formElement.config.width}
                                validationMsg={this.state.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default" onClick={this.props.unmountRegister}>Close</button>
                    <button type="submit" className="btn btn-primary">Save changes</button>
                </div>
            </form>
        );
        return (
            <div className="modal fade modal-danger" id="add-type-modal">
                <div className="modal-dialog modal-md">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Add New Product Type</h4>
                            <button type="button" className="close" onClick={this.props.unmountRegister} aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {form}
                    </div>
                </div>
            </div>

        )
    }
}


export default connect(null, { updateExistingColors })(RegisterProductType);
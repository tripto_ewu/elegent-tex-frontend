import React, { Component } from 'react'
import Input from '../../helpers/input';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import { formValid } from '../../helpers/formValidationChecker';
import moment from 'moment';
import { updateExistingOrders } from '../../../store/actions/orderAction';
import { toast } from 'react-toastify';

const phoneRegex = RegExp(
    /^[0][1-9]\d{9}$|^[1-9]\d{11}$/
);
class Checkout extends Component {

    state = {
        customerForm: {
            deleveryDate: {
                elementType: 'date',
                label: 'Delevery Date',
                elementConfig: {
                    dateFormat: 'd MMMM, yyyy',
                    placeholder: 'Select delevery date'
                },
                value: new Date(),
                width: 'col-md-4',
                validationMsg: '',
                touched: false
            },
            deleveryMethod: {
                elementType: 'select',
                label: 'Delevery Method',
                elementConfig: {
                    options: [{ value: 0, label: 'Sundorbon Currier' }, { value: 1, label: 'SA Paribahan Currier' }, { value: 2, label: 'Home Delevery' }],
                    placeholder: 'Delevery method'
                },
                value: '',
                width: 'col-md-4',
                validationMsg: '',
                touched: false
            },
            customerName: {
                elementType: 'input',
                label: 'Customer Name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter customer name'
                },
                value: '',
                validationMsg: '',
                width: 'col-md-4',
                touched: false
            },
            phone: {
                elementType: 'input',
                label: 'Phone Number',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter phone number'
                },
                value: '',
                validationMsg: '',
                width: 'col-md-4',
                touched: false
            },
            address: {
                elementType: 'input',
                label: 'Address (e.g: street/road)',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter address'
                },
                value: '',
                validationMsg: '',
                width: 'col-md-4',
                touched: false
            },
            division: {
                elementType: 'select',
                label: 'Division',
                elementConfig: {
                    options: [],
                    placeholder: 'Select Division'
                },
                value: '',
                width: 'col-md-4',
                validationMsg: '',
                touched: false
            },
            district: {
                elementType: 'select',
                label: 'District',
                elementConfig: {
                    options: [],
                    placeholder: 'Select District'
                },
                value: '',
                width: 'col-md-4',
                validationMsg: '',
                touched: false
            },
            upazila: {
                elementType: 'select',
                label: 'Upazila',
                elementConfig: {
                    options: [],
                    placeholder: 'Select Upazila'
                },
                value: '',
                width: 'col-md-4',
                validationMsg: '',
                touched: false
            },
            notes: {
                elementType: 'textarea',
                label: 'Notes',
                elementConfig: {
                    rows: "3",
                    placeholder: 'Please enter notes...'
                },
                value: '',
                width: 'col-md-4',
                validationMsg: '',
                touched: false
            },
            image: {
                elementType: 'multiFile',
                label: 'Upload Image',
                elementConfig: {
                    type: 'file',
                },
                file: '',
                validationMsg: '',
                width: 'col-md-6',
                touched: false
            },
        },
        cartItems: {}

    }

    getAllDivisions = () => {
        axios.get(process.env.REACT_APP_API_KEY + '/api/getAllDivisions').then(response => {
            let data = this.structureSelectOptions(response.data);
            const updatedRegForm = { ...this.state.customerForm }
            const updatedDivision = { ...updatedRegForm.division }
            const updatedElementConfig = { ...updatedDivision.elementConfig, options: data }
            updatedDivision['elementConfig'] = updatedElementConfig
            updatedRegForm['division'] = updatedDivision;
            this.setState({ customerForm: updatedRegForm });
        }).catch(err => {
            console.log(err);
        })
    }

    getSelectedDistricts = () => {
        const selectedDivision = this.state.customerForm.division.value.value
        axios.post(`${process.env.REACT_APP_API_KEY}/api/getSelectedDistricts?division_id=${selectedDivision}`).then(response => {
            let data = this.structureSelectOptions(response.data);
            const updatedRegForm = { ...this.state.customerForm }
            const updatedDistrict = { ...updatedRegForm.district }
            const updatedElementConfig = { ...updatedDistrict.elementConfig, options: data }
            updatedDistrict['elementConfig'] = updatedElementConfig
            updatedRegForm['district'] = updatedDistrict;
            this.setState({ customerForm: updatedRegForm });
        }).catch(err => {
            console.log(err);
        });
    }
    getSelectedUpazilas = () => {
        const selectedDistrict = this.state.customerForm.district.value.value
        axios.post(`${process.env.REACT_APP_API_KEY}/api/getSelectedUpazilas?district_id=${selectedDistrict}`).then(response => {
            let data = this.structureSelectOptions(response.data);
            const updatedRegForm = { ...this.state.customerForm }
            const updatedUpazila = { ...updatedRegForm.upazila }
            const updatedElementConfig = { ...updatedUpazila.elementConfig, options: data }
            updatedUpazila['elementConfig'] = updatedElementConfig
            updatedRegForm['upazila'] = updatedUpazila;
            this.setState({ customerForm: updatedRegForm });
        }).catch(err => {
            console.log(err);
        });
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'customerName':
                validationMsg = value.length < 1 ? "Please enter customer name" : '';
                return validationMsg;
            case 'address':
                validationMsg = value.length < 1 ? "Please enter customer address" : '';
                return validationMsg;
            case 'deleveryMethod':
            case 'deleveryDate':
            case 'division':
            case 'district':
            case 'upazila':
                validationMsg = value.length < 1 ? "Please select an option" : '';
                return validationMsg;
            case 'phone':
                validationMsg = !phoneRegex.test(value) ? "Enter valid phone number" : '';
                return validationMsg;
            default:
                return validationMsg;
        }

    }
    handleChange = (event, inputIdentifier) => {
        console.log(event.target.files[0]);
        // const updatedCustomerForm = { ...this.state.customerForm };
        // const updatedFormElement = { ...updatedCustomerForm[inputIdentifier] };
        // if (updatedFormElement.elementType === 'select') {
        //     updatedFormElement.value = event;
        //     updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value.label, inputIdentifier);
        //     switch (inputIdentifier) {
        //         case 'division':
        //             this.setState({ customerForm: updatedCustomerForm }, () => this.getSelectedDistricts());
        //             updatedCustomerForm['district'].value = '';
        //             updatedCustomerForm['upazila'].value = '';
        //             break;
        //         case 'district':
        //             this.setState({ customerForm: updatedCustomerForm }, () => this.getSelectedUpazilas());
        //             console.log(updatedCustomerForm['upazila'])
        //             break
        //         case 'upazila':
        //             this.setState({ customerForm: updatedCustomerForm });
        //             break
        //         default:
        //             break;
        //     }
        // } else if (updatedFormElement.elementType === 'date') {
        //     updatedFormElement.value = event;
        //     updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        // }
        // else {
        //     updatedFormElement.value = event.target.value;
        //     updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        // }
        // updatedFormElement.touched = true;

        // updatedCustomerForm[inputIdentifier] = updatedFormElement;
        // console.log(updatedCustomerForm)
        // this.setState({ customerForm: updatedCustomerForm });

    }

    structureSelectOptions = (data) => {
        let options = [];
        data.forEach(function (element) {
            let temp_division = {
                value: element.id,
                label: element.name
            }
            options.push(temp_division)
        });
        return options;
    }

    submitOrder = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {};
        let products = [];
        for (let formElementIdentifier in this.state.customerForm) {
            if (formElementIdentifier !== 'notes') {
                if (this.state.customerForm[formElementIdentifier].elementType === 'select' &&
                    this.state.customerForm[formElementIdentifier].value !== '') {
                    formData[formElementIdentifier] = this.state.customerForm[formElementIdentifier].value.label;
                } else if (this.state.customerForm[formElementIdentifier].elementType === 'date') {
                    let value = formData[formElementIdentifier] = this.state.customerForm[formElementIdentifier].value
                    formData[formElementIdentifier] = moment(value).format('DD-MM-YYYY');
                }
                else {
                    formData[formElementIdentifier] = this.state.customerForm[formElementIdentifier].value
                }
                formError[formElementIdentifier] = this.state.customerForm[formElementIdentifier].validationMsg;
            }

        }
        this.state.cartItems.products.forEach(element => {
            let product = {};
            // console.log(element)
            product['product_id'] = element.product.value;
            product['productColor'] = element.productColors.label;
            product['productType'] = element.productTypes.label;
            product['quantity'] = element.quantity;
            product['price'] = element.price;
            products.push(product);
        });

        formData['products'] = products
        formData['totalAmount'] = this.state.cartItems.totalAmount;

        //console.log(formData)
        //console.log(formError)

        if (formValid(formError, formData)) {
            formData['notes'] = this.state.customerForm['notes'].value
            formData['userId'] = this.props.userInfo.id
            axios.post(process.env.REACT_APP_API_KEY + '/api/createNewOrder', formData).then(response => {
                // this.props.history.push({
                //     pathname: '/template',
                //     state: { detail: response.data }
                // })
                toast.success("Order Created Successfully !!!");

            }).catch(err => {
                //this.props.returnErrors(err.response.data.message, err.response.status, 'REGISTER_FAIL');
                console.log(err)
            })
        }

    }

    // listen = () => {
    //     window.Echo.channel('orders').listen('NewOrder', (order) => {
    //         this.props.updateExistingOrders(order, 'add');
    //     })
    // }

    componentDidMount() {
        let items = localStorage.getItem('cartItems');
        console.log(JSON.parse(items))
        this.setState({ cartItems: JSON.parse(items) })
        this.getAllDivisions();
    }
    componentDidUpdate() {
        console.log(this.props)
    }
    componentWillUnmount() {
        console.log("Unmount")
        localStorage.removeItem('cartItems')
    }
    render() {
        const orderData = JSON.parse(localStorage.getItem('cartItems'));


        const formElementsArray = [];
        for (let key in this.state.customerForm) {
            formElementsArray.push({
                id: key,
                config: this.state.customerForm[key]
            });
        }
        if (!orderData) {
            return <Redirect to='/createNewOrder' />;
        }
        let form = (

            <form>
                <div className="card-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {

                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                width={formElement.config.width}
                                label={formElement.config.label}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                // imageKey={formElement.config.imageKey}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                validationMsg={formElement.config.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                    <div className="row">
                        <div className="col-12 table-responsive">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Product Type</th>
                                        <th>Product Color</th>
                                        <th>Qty</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {orderData.products.map((order, index) => {
                                        return <tr key={index}>

                                            <td>{order.product.label}</td>
                                            <td>{order.productTypes.label}</td>
                                            <td>{order.productColors.label}</td>
                                            <td>{order.quantity}</td>
                                            <td>{order.price}tk</td>
                                        </tr>
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <h5>Inputs</h5>
                        </div>
                        <div className="col-6">
                            <p className="lead">Amount Due 2/22/2014</p>
                            <div className="table-responsive">
                                <table className="table">
                                    <tbody><tr>
                                        <th>Total:</th>
                                        <td>{orderData.totalAmount}tk</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="row no-print">
                        <div className="col-12">
                            <button type="button"
                                onClick={this.submitOrder}
                                className="btn btn-success float-right">
                                <i className="far fa-credit-card" /> Submit Order</button>
                        </div>
                    </div>
                </div>
            </form>

        );


        return (
            <div className="content-wrapper">
                <div className="content ">
                    <div className="container pt-4">
                        <div className="col-12">
                            <div className="card card-success">
                                <div className="card-header">
                                    <h3 className="card-title">Additional INFO</h3>
                                </div>
                                {form}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    orderCart: state.orders.orderCart,
    userInfo: state.auth.user
})

export default connect(mapStateToProps, { updateExistingOrders })(Checkout);

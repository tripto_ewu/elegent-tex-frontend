import React, { Component } from 'react'
import Input from '../../helpers/input';
import ContentHeader from '../../extras/ContentHeader';
import { connect } from 'react-redux';
import { loadProduct, loadProductColor } from '../../../store/actions/productActions';
import OrderCart from './OrderCart';
import { formValid } from '../../helpers/formValidationChecker';
import { toast } from 'react-toastify';
class CreateOrder extends Component {

    constructor(props) {
        super(props)

        this.state = {
            orderForm: {
                product: {
                    elementType: 'select',
                    label: 'Product',
                    elementConfig: {
                        options: [],
                        placeholder: 'Select product'
                    },
                    value: '',
                    width: 'col-md-4',
                    validationMsg: '',
                    touched: false
                },
                productTypes: {
                    elementType: 'select',
                    label: 'Product Type',
                    elementConfig: {
                        options: [],
                        placeholder: 'Select product type'
                    },
                    value: '',
                    width: 'col-md-4',
                    validationMsg: '',
                    touched: false
                },
                productColors: {
                    elementType: 'select',
                    label: 'Product Color',
                    elementConfig: {
                        options: this.props.productColors,
                        placeholder: 'Select product color'
                    },
                    value: '',
                    width: 'col-md-4',
                    validationMsg: '',
                    touched: false
                },
                quantity: {
                    elementType: 'input',
                    label: 'Quantity',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Please enter product Quantity'
                    },
                    value: '',
                    width: 'col-md-4',
                    validationMsg: '',
                    touched: false
                },
                price: {
                    elementType: 'input',
                    label: 'Price',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Please enter price'
                    },
                    value: '',
                    width: 'col-md-4',
                    validationMsg: '',
                    touched: false
                },
            },
            orders: [],
            totalAmount: 0

        }
    }



    structureSelectOptions = (el, data, attr) => {
        let options = [];
        data.forEach(function (element) {
            let temp = {
                value: element['id'],
                label: element[attr]
            }
            options.push(temp)
        });
        const updatedOrderForm = { ...this.state.orderForm }
        const updatedOrderFormElement = { ...updatedOrderForm[el] }
        const updatedOrderFormElementConfig = { ...updatedOrderFormElement['elementConfig'] }
        updatedOrderFormElementConfig.options = options
        updatedOrderFormElement['elementConfig'] = updatedOrderFormElementConfig;
        updatedOrderForm[el] = updatedOrderFormElement;
        this.setState({ orderForm: updatedOrderForm })
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'product':
                validationMsg = value.length < 1 ? "Enter Product Name" : '';
                return validationMsg;
            case 'productType':
                validationMsg = value.length < 1 ? "Enter Product Name" : '';
                return validationMsg;
            case 'price':
                validationMsg = value.length < 1 ? "Price is required" : '';
                return validationMsg;
            case 'quantity':
                validationMsg = value.length < 1 ? "Product quantity is required" : '';
                return validationMsg;
            default:
                return validationMsg;
        }
    }

    handleChange = (event, inputIdentifier) => {
        const updatedOrderForm = { ...this.state.orderForm };
        const updatedFormElement = { ...updatedOrderForm[inputIdentifier] };
        if (updatedFormElement.elementType === 'select') {
            // if (inputIdentifier === 'product') {
            //     if (event.label === 'Sofa Cover') {
            //         let form = {
            //             elementType: 'input',
            //             label: 'Ratio',
            //             elementConfig: {
            //                 type: 'text',
            //                 placeholder: 'Please enter ratio'
            //             },
            //             value: '',
            //             width: 'col-md-4',
            //             validationMsg: '',
            //             touched: false
            //         }
            //         updatedOrderForm['sofaType'] = form;
            //     } else {
            //         delete updatedOrderForm.sofaType;
            //     }
            // }
            updatedFormElement.value = event;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value.label, inputIdentifier);
        }
        else {
            updatedFormElement.value = event.target.value;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        }
        updatedFormElement.touched = true;

        updatedOrderForm[inputIdentifier] = updatedFormElement;
        this.setState({ orderForm: updatedOrderForm });

    }

    addOrderToCart = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {}
        let amount = 0;
        for (let formElementIdentifier in this.state.orderForm) {
            if (this.state.orderForm[formElementIdentifier].elementType === 'select') {
                formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
            }
            else {
                formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value
            }
            formError[formElementIdentifier] = this.state.orderForm[formElementIdentifier].validationMsg;

        }
        amount = this.state.totalAmount + parseInt(this.state.orderForm['price'].value);

        const updatedOrders = [...this.state.orders];
        updatedOrders.push(formData);
        if (formValid(formError, formData)) {
            this.setState({ orders: updatedOrders, totalAmount: amount }, () => {
                const updatedOrderForm = { ...this.state.orderForm };
                for (let formElementIdentifier in this.state.orderForm) {
                    const updatedFormElement = { ...updatedOrderForm[formElementIdentifier] };
                    updatedFormElement.value = '';
                    updatedFormElement.validationMsg = '';
                    updatedOrderForm[formElementIdentifier] = updatedFormElement;
                    this.setState({ orderForm: updatedOrderForm });
                }
            })
        } else {
            toast.error("Please fill all the fields");
        }




    }

    componentDidMount() {
        this.props.loadProduct();
        this.props.loadProductColor();
    }
    componentDidUpdate(previousProps, previousState) {

        if (previousProps.products !== this.props.products) {
            this.structureSelectOptions('product', this.props.products, 'product_name');
        }
        if (previousProps.productColors !== this.props.productColors) {
            this.structureSelectOptions('productColors', this.props.productColors, 'color_name');
        }
        if (this.state.orderForm.product.value && this.state.orderForm.productTypes === previousState.orderForm.productTypes) {
            let index = this.props.products.findIndex(x => x.id === this.state.orderForm.product.value.value);
            this.structureSelectOptions('productTypes', this.props.products[index].types, 'type_name');

        }
    }




    render() {
        const formElementsArray = [];
        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }
        let form = (
            <form onSubmit={this.addOrderToCart}>
                <div className="card-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {

                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                width={formElement.config.width}
                                label={formElement.config.label}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                // imageKey={formElement.config.imageKey}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                validationMsg={formElement.config.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="card-footer clearfix">
                    <button type="submit" className="btn btn-primary btn-sm float-right">Add to cart</button>
                </div>
            </form>

        );
        return (
            <div className="content-wrapper">
                <section className="content">
                    <div className="container-fluid">
                        <div className="row pt-3">
                            <div className="col-md-8">
                                {/* general form elements */}
                                <div className="card card-primary">
                                    <div className="card-header">
                                        <h3 className="card-title">Order Form</h3>
                                    </div>
                                    {form}
                                </div>
                            </div>
                            <div className="col-md-4">
                                <OrderCart
                                    totalAmount={this.state.totalAmount}
                                    orders={this.state.orders} />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    products: state.products.products,
    productColors: state.products.productColors
})
export default connect(mapStateToProps, { loadProduct, loadProductColor })(CreateOrder);

import React, { Component } from 'react';
import ContentHeader from '../../extras/ContentHeader';
import CreateOrder from './CreateOrder';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { loadAllOrders, updateExistingOrders } from '../../../store/actions/orderAction';
import Loading from '../../extras/Loading';
import { Link } from 'react-router-dom'


class Orders extends Component {

    state = {
        data: [],
        url: '/api/getAllOrders',
        pagination: {}

    }

    componentDidMount() {
        this.props.loadAllOrders(this.state.url, this.makePagination);
        console.log("Component Did mount")
        window.Echo.channel('orders').listen('NewOrder', (order) => {
            this.props.updateExistingOrders(order)
        })
    }

    componentDidUpdate(prevProp) {
        if (prevProp.orders !== this.props.orders) {
            this.setState({ data: this.props.orders.data })
        }
        console.log(this.props)
    }

    makePagination = (data) => {
        let pagination = {
            current_page: data.current_page,
            last_page: data.last_page,
            next_page_url: data.next_page_url ? data.next_page_url.replace(process.env.REACT_APP_API_KEY, "") : data.next_page_url,
            prev_page_url: data.prev_page_url ? data.prev_page_url.replace(process.env.REACT_APP_API_KEY, "") : data.prev_page_url,
        }
        this.setState({ pagination: pagination });
    }

    loadNextPage = () => {
        if (this.state.pagination.next_page_url) {
            this.setState({ url: this.state.pagination.next_page_url }, () => {
                this.props.loadAllOrders(this.state.url, this.makePagination);
            });
        }
    }
    loadPrevPage = () => {
        if (this.state.pagination.prev_page_url) {
            this.setState({ url: this.state.pagination.prev_page_url }, () => {
                this.props.loadAllOrders(this.state.url, this.makePagination);
            });
        }
    }

    render() {

        const orders = this.state.data;
        console.log(orders)
        const loading = (<Loading />)
        const ordersTable = (
            orders.map((order) => {
                return <tr key={order.id}>
                    <td>
                        <Link
                            to={{
                                pathname: `/createNewOrder/invoice/${order.id}`,
                                state: order
                            }}> ORD{order.id}
                        </Link>
                    </td>
                    <td>{order.products.map(product => {
                        return <ul key={product.id} style={{ padding: "0px 0px 0px 16px", marginBottom: "0px" }}>
                            <li>{product.name}</li>
                        </ul>
                    })}</td>
                    <td>{order.delevery_date}</td>
                    <td>{order.user}</td>
                    <td><small className="badge badge-danger">{order.status}</small></td>
                    <td>{order.total_amount} tk</td>
                    <td className="project-actions text-right ">
                        <button className="btn btn-info btn-sm mx-1" >
                            <i className="fas fa-pencil-alt">
                            </i></button>
                        <button className="btn btn-danger btn-sm " data-toggle="modal"
                            data-target="#warning-modal" >
                            <i className="fas fa-trash">
                            </i></button>
                    </td>
                </tr >
            })

        )
        return (
            <div className="content-wrapper" >
                {/* <ContentHeader name="Orders" /> */}
                <section className="content">
                    <div className="container-fluid">
                        <div className="row mx-1 pt-3">
                            <div className="card">
                                <div className="card-header border-transparent">
                                    <h3 className="card-title">Latest Orders</h3>
                                    <div className="card-tools">
                                        <ul className="pagination pagination-sm float-right">
                                            <li className="page-item">
                                                <a className="page-link" href="#" onClick={this.loadPrevPage}>«</a>
                                            </li>
                                            <li className="page-item">
                                                <a className="page-link" href="#" onClick={this.loadNextPage}>»</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                {/* /.card-header */}
                                <div className="card-body table-responsive p-0">
                                    <div className="table table-head-fixed text-nowrap">
                                        <table className="table m-0 table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Products</th>
                                                    <th>Delevery Date</th>
                                                    <th>Ordered By</th>
                                                    <th>Status</th>
                                                    <th>Total Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {ordersTable}
                                            </tbody>
                                        </table>
                                    </div>
                                    {/* /.table-responsive */}
                                </div>
                                {/* /.card-body */}
                                <div className="card-footer clearfix">
                                    {/* <a href="!#" className="btn btn-sm btn-info float-left">Place New Order</a> */}
                                    <NavLink to="/createNewOrder" exact activeclassname="active" className="btn btn-sm btn-info float-left" >
                                        Place New Order
                                        </NavLink>
                                    <a href="!#" className="btn btn-sm btn-secondary float-right">View All Orders</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div >
        )
    }
}
const mapStateToProps = state => ({
    orders: state.orders.orders,
    isLoading: state.orders.isLoading
})

export default connect(mapStateToProps, { loadAllOrders, updateExistingOrders })(Orders);
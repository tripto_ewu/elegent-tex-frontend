import React, { Component } from 'react'
import { connect } from 'react-redux';
import { loadAllOrders } from '../../../store/actions/orderAction';
import moment from 'moment';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

class Invoice extends Component {


    // _exportPdf = () => {

    //     html2canvas(document.querySelector("#capture")).then(canvas => {

    //         const imgData = canvas.toDataURL('image/png');

    //         var pdf = new jsPDF("l", "mm", "a4");
    //         var width = pdf.internal.pageSize.getWidth();
    //         var height = pdf.internal.pageSize.getHeight();
    //         pdf.addImage(imgData, 'JPEG', 0, 0, width, height);
    //         //pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
    //         //pdf.save('download.pdf')
    //         //pdf.save("download.pdf");
    //         pdf.autoPrint();
    //         window.open(pdf.output('bloburl'), '_blank');
    //     });

    // }

    componentDidMount() {
        console.log(this.props.location.state)
    }
    render() {
        let order = this.props.location.state
        return (
            <div className="content-wrapper" >
                {/* <ContentHeader name="Orders" /> */}
                <section className="content">
                    <div className="container-fluid">
                        <div className="invoice p-3 mb-3" id="capture">
                            {/* title row */}
                            <div className="row">
                                <div className="col-12">
                                    <h4>
                                        <i className="fas fa-globe" /> Elegent Tex Ltd.
                                        <small className="float-right">Date: {moment(order.created_at).format('DD-MM-YYYY')}</small>
                                    </h4>
                                </div>
                                {/* /.col */}
                            </div>
                            {/* info row */}
                            <div className="row invoice-info">
                                <div className="col-sm-4 invoice-col">
                                    From
                            <address>
                                        <strong>Admin, Inc.</strong><br />
                                795 Folsom Ave, Suite 600<br />
                                San Francisco, CA 94107<br />
                                Phone: (804) 123-5432<br />
                                Email: info@almasaeedstudio.com
                            </address>
                                </div>
                                {/* /.col */}
                                <div className="col-sm-4 invoice-col">
                                    To
                                    <address>
                                        <strong>{order.customer.name}</strong><br />
                                        {order.customer.address}<br />
                                        {order.customer.upazila}, {order.customer.district}, {order.customer.division}<br />
                                        Phone: {order.customer.phone_no}
                                    </address>
                                </div>
                                {/* /.col */}
                                <div className="col-sm-4 invoice-col">
                                    <b>Invoice #007612</b><br />
                                    <br />
                                    <b>Order ID:</b>ORD{order.id}<br />
                                    <b>Delivery Date:</b> {order.delevery_date}<br />
                                </div>
                                {/* /.col */}
                            </div>
                            {/* /.row */}
                            {/* Table row */}
                            <div className="row">
                                <div className="col-12 table-responsive">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Product Type</th>
                                                <th>Color</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {order.products.map(product => {
                                                return <tr key={product.id}>
                                                    <td>{product.name}</td>
                                                    <td>{product.productType}</td>
                                                    <td>{product.productColor}</td>
                                                    <td>{product.quantity} pc</td>
                                                    <td>{product.price}Tk</td>
                                                </tr>
                                            })
                                            }


                                        </tbody>
                                    </table>
                                </div>
                                {/* /.col */}
                            </div>
                            {/* /.row */}
                            <div className="row">
                                {/* accepted payments column */}
                                <div className="col-6">
                                    <p className="lead">Note: {order.note}</p>
                                </div>
                                {/* /.col */}
                                <div className="col-6">
                                    <div className="table-responsive">
                                        <table className="table">
                                            <tbody><tr>
                                                <th style={{ width: '50%' }}>Delivery method:</th>
                                                <td>{order.delevery_mathod}</td>
                                            </tr>
                                                <tr>
                                                    <th>Total:</th>
                                                    <td><span className="text-danger">{order.total_amount}Tk</span></td>
                                                </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                                {/* /.col */}
                            </div>
                            {/* /.row */}
                            {/* this row will not appear when printing */}
                            <div className="row no-print">
                                <div className="col-12">
                                    <a href="invoice-print.html" rel="noopener" target="_blank" className="btn btn-default"><i className="fas fa-print" /> Print</a>

                                    <button type="button" className="btn btn-success float-right" style={{ marginRight: 5 }} onClick={this._exportPdf}>
                                        <i className="fas fa-download" /> Generate PDF
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        )
    }
}
const mapStateToProps = state => ({
    orders: state.orders.orders,
})

export default connect(mapStateToProps, { loadAllOrders })(Invoice)
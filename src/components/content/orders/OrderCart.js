import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { orderCart } from '../../../store/actions/orderAction';

class OrderCart extends Component {

    state = {
        totalAmount: 0
    }

    orderCart = (items) => {
        localStorage.setItem('cartItems', JSON.stringify(items));
    }

    render() {
        let orderCartItems = {
            products: this.props.orders,
            totalAmount: this.props.totalAmount
        }
        let total = 0;
        return (
            <div className="card card-outline card-info" style={{ minHeight: "313px" }}>
                <div className="card-header">
                    <h2 className="card-title text-info"><strong>Cart</strong></h2>
                </div>
                <div className="card-body p-0">

                    {this.props.orders.length !== 0 ? this.props.orders.map((order, index) => {
                        //let image = URL.createObjectURL(order.image)

                        return <ul className="products-list product-list-in-card pl-2 pr-2" key={index}>
                            <li className="item" key={index}>
                                <div className="px-2">
                                    <div className="product-title px-1">{order.product.label}
                                        <span className="badge badge-warning float-right mt-2">{`${order.price} tk`}</span></div>
                                    <span className="product-description px-1">
                                        {order.productColors.label}, {order.productTypes.label}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    }) : <div className="text-center my-3 p-2">
                            <span className="my-2"><i className="fas fa-cart-plus fa-7x mb-3" style={{ color: "#dbdbdb" }}></i><br></br></span>
                            <h5 className="text-danger">No items in the cart</h5>
                        </div>}
                </div>
                <div className="card-footer text-center">
                    {this.props.totalAmount !== 0 ?
                        <React.Fragment
                        ><h6 className="product-title">
                                Total Amount :
                                <span className="text-danger"> {this.props.totalAmount} tk</span></h6> <hr></hr>
                        </React.Fragment> : null}

                    {this.props.orders.length ?
                        <Link to={{ pathname: "/createNewOrder/checkout", data: this.props }}
                            onClick={() => this.orderCart(orderCartItems)} >Checkout
                    </Link> : null}
                </div>
            </div >

        )
    }
}




export default connect(null, { orderCart })(OrderCart);

import React, { Component } from 'react'
import RegisterProduct from './RegisterProduct';
import UpdateProduct from './UpdateProduct';
import { connect } from 'react-redux';
import axios from 'axios';
import WarningModal from '../../extras/WarningModal';
import { toast } from 'react-toastify';
import { loadProduct, updateExistingProducts } from '../../../store/actions/productActions';

toast.configure();

class AllProducts extends Component {

    state = {
        products: [],
        selectedProduct: '',
        unmountUpdateProduct: true,
        unmountRegisterProduct: true,
        unmountWarningModal: true,
    }

    deleteProduct = (product) => {
        this.handleWarningModal()
        this.setState({ selectedProduct: product });

    }
    confirmDeleteProduct = () => {
        const productId = this.state.selectedProduct.id
        axios.get(`${process.env.REACT_APP_API_KEY}/api/deleteProduct/${productId}`).then(response => {
            // this.props.updateColor(response.data);
            // toast.success("Color is updated !!!");
            window.$("#warning-modal").modal("hide");
            this.handleWarningModal();
            this.props.updateExistingProducts(response.data, 'delete')
            console.log(response);
        }).catch(err => {

            toast.success("Color Update Error!!!");
        })
    }
    updateProductArray = (product, operation) => {
        console.log(product)
        console.log(operation)
        const updatedProducts = [...this.state.products];
        var foundIndex = updatedProducts.findIndex(x => x.id === product.id);
        if (foundIndex !== -1) {
            operation === 'update' ? updatedProducts[foundIndex] = product : updatedProducts.splice(foundIndex, 1);
        } else {
            updatedProducts.unshift(product)
        }
        this.setState({ products: updatedProducts, selectedProduct: null });
    }

    updateProduct = (product) => {
        this.setState({ selectedProduct: product, unmountUpdateProduct: !this.state.unmountUpdateProduct })
    }
    handleUnmountUpdate = () => {
        this.setState({ unmountUpdateProduct: !this.state.unmountUpdateProduct })
    }
    handleWarningModal = () => {
        this.setState({ unmountWarningModal: !this.state.unmountWarningModal })
    }
    componentDidMount() {
        //this.getAllProducts();
        this.props.loadProduct();

    }


    render() {
        const productsArray = this.props.products;
        console.log(this.props.products)
        let products = (productsArray ? productsArray.map((product) => {
            console.log(product)
            return <tr key={product.id}>
                <td><a href="!#">{`PR${product.id}`}</a></td>
                <td>{product.product_name}</td>
                <td>{product.types.map(type => {
                    return <span key={type.id} className="badge badge-secondary right">{type.type_name}</span>
                })}</td>
                <td className="project-state">
                    {product.is_active ? <span className="badge badge-success">Active</span> : <span className="badge badge-danger">Inactive</span>}
                </td>
                <td className="project-actions text-right ">
                    <button className="btn btn-info btn-sm mx-1" onClick={() => this.updateProduct(product)}>
                        <i className="fas fa-pencil-alt">
                        </i></button>
                    <button className="btn btn-danger btn-sm " data-toggle="modal"
                        data-target="#warning-modal" onClick={() => this.deleteProduct(product)}>
                        <i className="fas fa-trash">
                        </i></button>
                </td>
            </tr>
        }) : <tr>Loading...</tr>)
        return (
            <div className="col-md-6">
                <div className="card">
                    <div className="card-header border-transparent bg-info">
                        <h2 className="card-title">Products</h2>
                    </div>
                    {/* /.card-header */}
                    <div className="card-body p-0">
                        <div className="table-responsive">
                            <table className="table m-0">
                                <thead>
                                    <tr>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>Product Types</th>
                                        <th>Active</th>
                                        <th className="text-right">Action</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    {products}
                                </tbody>
                            </table>
                        </div>
                        {/* /.table-responsive */}
                    </div>
                    {/* /.card-body */}
                    {!this.state.unmountRegisterProduct ?
                        <RegisterProduct
                            newProduct={this.props.updateExistingProducts}
                            unmountRegister={() => this.setState({ unmountRegisterProduct: !this.state.unmountRegisterProduct })} /> : null}
                    {!this.state.unmountUpdateProduct ?
                        <UpdateProduct updateProduct={this.props.updateExistingProducts}
                            unmountUpdateProduct={this.handleUnmountUpdate}
                            selectedProduct={this.state.selectedProduct} /> : null}
                    <div className="card-footer clearfix">
                        <button
                            className="btn btn-sm btn-info float-left"
                            onClick={(e) => this.setState({ unmountRegisterProduct: !this.state.unmountRegisterProduct })}>
                            New Product</button>
                    </div>
                    {/* /.card-footer */}
                </div>

                {!this.state.unmountWarningModal ? <WarningModal
                    unmountModal={this.handleWarningModal}
                    message="Warning!!! Are You Sure?"
                    action={this.confirmDeleteProduct}
                /> : null}

            </div>
        )
    }
}
const mapStateToProps = state => ({
    products: state.products.products,
    error: state.error
})
export default connect(mapStateToProps, { loadProduct, updateExistingProducts })(AllProducts);
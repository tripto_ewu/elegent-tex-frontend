import React, { Component } from 'react';
import ContentHeader from '../../extras/ContentHeader';
import AllProducts from './AllProducts'

export default class Products extends Component {
    render() {
        return (
            <div className="content-wrapper">
                <ContentHeader name="Products" />
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <AllProducts />
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

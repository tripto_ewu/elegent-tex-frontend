import React, { Component } from 'react'
import axios from 'axios';
import { toast } from 'react-toastify';
import Input from '../../helpers/input';
import { connect } from 'react-redux';
import { loadProductTypes } from '../../../store/actions/productActions';
import { formValid } from '../../helpers/formValidationChecker'
class RegisterProduct extends Component {

    state = {
        addProductForm: {
            productName: {
                elementType: 'input',
                label: 'Product name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Product name'
                },
                value: '',
                validationMsg: '',
                width: 'col-md-12',
                touched: false
            },
            productType: {
                elementType: 'select',
                label: 'Product Type',
                elementConfig: {
                    options: this.props.productTypes,
                    isMulti: true,
                    placeholder: 'Select product Type'
                },
                value: '',
                width: 'col-md-12',
                validationMsg: '',
                touched: false
            },
        }
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'productName':
                validationMsg = value.length < 1 ? "Enter Product Name" : '';
                return validationMsg;
            default:
                return validationMsg;
        }

    }

    handleChange = (event, inputIdentifier) => {

        console.log(event)
        const updatedAddProductForm = { ...this.state.addProductForm };
        const updatedFormElement = { ...updatedAddProductForm[inputIdentifier] };
        console.log(updatedFormElement)
        if (updatedFormElement.elementType === 'select') {

            updatedFormElement.value = event;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value.label, inputIdentifier);

        } else {
            updatedFormElement.value = event.target.value;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        }
        updatedFormElement.touched = true;


        console.log(updatedFormElement);
        updatedAddProductForm[inputIdentifier] = updatedFormElement;
        this.setState({ addProductForm: updatedAddProductForm });
    }

    structureSelectOptions = (el, data, attr) => {
        let options = [];
        data.forEach(function (element) {
            let temp = {
                value: element['id'],
                label: element[attr]
            }
            options.push(temp)
        });
        const updatedForm = { ...this.state.addProductForm }
        const updatedFormElement = { ...updatedForm[el] }
        console.log(updatedFormElement)
        const updatedFormElementConfig = { ...updatedFormElement['elementConfig'] }
        updatedFormElementConfig.options = options
        updatedFormElement['elementConfig'] = updatedFormElementConfig;
        updatedForm[el] = updatedFormElement;
        this.setState({ addProductForm: updatedForm })
    }

    registerProduct = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {};

        const productTypes = []
        for (let formElementIdentifier in this.state.addProductForm) {

            if (this.state.addProductForm[formElementIdentifier].elementType === 'select') {
                for (let i in this.state.addProductForm[formElementIdentifier].value) {
                    productTypes.push(this.state.addProductForm[formElementIdentifier].value[i].value);
                    formData[formElementIdentifier] = productTypes
                }
                formData[formElementIdentifier] = productTypes
            } else {
                formData[formElementIdentifier] = this.state.addProductForm[formElementIdentifier].value
            }

            formError[formElementIdentifier] = this.state.addProductForm[formElementIdentifier].validationMsg;
        }

        console.log(formData);
        if (formValid(formError, formData)) {
            axios.post(process.env.REACT_APP_API_KEY + '/api/addProduct', formData).then(response => {
                this.props.newProduct(response.data, 'add');
                console.log(response.data);
                toast.success("House Registered Successfully !!!");
                window.$("#add-product-modal").modal("hide");
                this.props.unmountRegister();
            }).catch(err => {
                //this.props.returnErrors(err.response.data.message, err.response.status, 'REGISTER_FAIL');
                console.log(err)
            })
        }
        else {
            toast.error("Please fill all the required fields !!!");
        }
        if (this.state.errorMsg) {
            setTimeout(function () { this.props.clearErrors() }, 3000);
        }
        console.log(this.state)
    }
    componentDidMount() {
        window.$("#add-product-modal").modal("show");
        this.props.loadProductTypes();
    }

    componentDidUpdate(previousProps) {
        if (previousProps.productTypes !== this.props.productTypes) {
            this.structureSelectOptions('productType', this.props.productTypes, 'type_name');
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.addProductForm) {
            formElementsArray.push({
                id: key,
                config: this.state.addProductForm[key]
            });
        }
        let form = (
            <form onSubmit={this.registerProduct}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {
                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                label={formElement.config.label}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                width={formElement.config.width}
                                validationMsg={formElement.config.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default btn-sm" data-dismiss="modal" onClick={this.props.unmountRegister}>Close</button>
                    <button type="submit" className="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        )
        return (
            <div className="modal fade" data-keyboard="false" data-backdrop="static" id="add-product-modal">
                <div className="modal-dialog modal-md">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Add New Product</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.unmountRegister}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {form}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    productTypes: state.products.productTypes,
    error: state.error
})
export default connect(mapStateToProps, { loadProductTypes })(RegisterProduct);

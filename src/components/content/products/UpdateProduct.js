import React, { Component } from 'react';
import Input from '../../helpers/input';
import { toast } from 'react-toastify';
import axios from 'axios';
import { formValid } from '../../helpers/formValidationChecker';

export default class UpdateProduct extends Component {

    state = {
        updateProductForm: {
            productName: {
                elementType: 'input',
                label: 'Product name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Product name'
                },
                value: this.props.selectedProduct.product_name,
                validationMsg: '',
                width: 'col-md-12',
                touched: false
            },
            isActive: {
                elementType: 'select',
                label: 'Product status',
                elementConfig: {
                    options: [{ value: 0, label: 'Inactive' }, { value: 1, label: 'Active' }],
                    placeholder: 'Select color availability'
                },
                value: { value: this.props.selectedProduct.is_active, label: this.props.selectedProduct.is_active == 0 ? 'Inactive' : 'Active' },
                validationMsg: '',
                touched: false
            },
        }
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'productName':
                validationMsg = value.length < 1 ? "Enter Product Name" : '';
                return validationMsg;
            default:
                break;
        }

    }

    handleChange = (event, inputIdentifier) => {
        const updatedProductForm = { ...this.state.updateProductForm };
        const updatedFormElement = { ...updatedProductForm[inputIdentifier] };
        if (updatedFormElement.elementType === 'select') {
            updatedFormElement.value = event;
            updatedFormElement.valid = this.checkFormValidity(updatedFormElement.value.label, inputIdentifier);
        } else {
            updatedFormElement.value = event.target.value;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        }
        updatedFormElement.touched = true;

        updatedProductForm[inputIdentifier] = updatedFormElement;
        let formIsvalid = true;
        for (let inputIdentifier in updatedProductForm) {
            formIsvalid = updatedProductForm[inputIdentifier].valid && formIsvalid;
        }
        this.setState({ updateProductForm: updatedProductForm, formIsvalid: formIsvalid });

    }

    updateProduct = (event) => {
        event.preventDefault();
        const formData = {};
        const formError = {}
        for (let formElementIdentifier in this.state.updateProductForm) {
            if (this.state.updateProductForm[formElementIdentifier].elementType === 'select') {
                formData[formElementIdentifier] = this.state.updateProductForm[formElementIdentifier].value.value;
            } else {
                formData[formElementIdentifier] = this.state.updateProductForm[formElementIdentifier].value
            }
            formError[formElementIdentifier] = this.state.updateProductForm[formElementIdentifier].validationMsg;
        }
        const id = this.props.selectedProduct.id;
        if (formValid(formError, formData)) {
            axios.post(`${process.env.REACT_APP_API_KEY}/api/updateProduct/${id}`, formData).then(response => {
                this.props.updateProduct(response.data, 'update');
                toast.success("Product is updated !!!");
                window.$("#update-product-modal").modal("hide");
                this.props.unmountUpdateProduct();
            }).catch(err => {

                toast.success("Product Update Error!!!");
            })
        }
        else {
            toast.error("Please fill all the required fields !!!");
        }

    }
    componentDidMount() {
        window.$('#update-product-modal').modal('show');
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.updateProductForm) {
            formElementsArray.push({
                id: key,
                config: this.state.updateProductForm[key]
            });
        }
        let form = (
            <form onSubmit={this.updateProduct}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {

                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                label={formElement.config.label}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                width={formElement.config.width}
                                invalid={!formElement.config.valid}
                                touched={formElement.config.touched}
                                validationMsg={formElement.config.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default btn-sm" data-dismiss="modal" onClick={this.props.unmountUpdateProduct}>Close</button>
                    <button type="submit" className="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        );
        return (
            <div className="modal fade" data-keyboard="false" data-backdrop="static" id="update-product-modal">
                <div className="modal-dialog modal-md">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Edit Color</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.unmountUpdateProduct}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {form}
                    </div>
                </div>
            </div>

        )
    }
}

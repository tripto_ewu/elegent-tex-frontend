import React, { useEffect } from 'react';

export default function WarningModal(props) {

    useEffect(() => {
        window.$('#warning-modal').modal('show');
    }, []);
    return (
        <div className="modal fade" data-keyboard="false" data-backdrop="static" id="warning-modal">
            <div className="modal-dialog modal-dialog-centered modal-md">
                <div className="modal-content">
                    <div className="modal-header bg-danger">
                        <h4 className="modal-title">Delete Color</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={props.unmountModal}>
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <h6>{props.message}</h6>
                    </div>
                    <div className="modal-footer justify-content-between">
                        <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal" onClick={props.unmountModal}>Close</button>
                        <button type="button" className="btn btn-danger btn-sm" onClick={props.action}>Delete</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

import React from 'react'
import './extras.css'

export default function ContentHeader(props) {
    return (
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-12 col-md-12">
                        <div className="info-box gradient" style={{ minHeight: "64px" }}><h1 className="m-2 text-light">{props.name}</h1></div>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            {/* <li className="breadcrumb-item"><a href="#">Home</a></li> */}
                            {/* <li className="breadcrumb-item active">{this.props.location.pathname}</li> */}
                        </ol>
                    </div>
                </div>
            </div>{/* /.container-fluid */}
        </section>
    )
}

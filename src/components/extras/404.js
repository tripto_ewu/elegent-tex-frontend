import React from 'react'

export default function Content() {
    return (

        <div className="login-page">
            <section className="content">
                <div className="error-page">
                    <h2 className="headline text-danger">500</h2>
                    <div className="error-content">
                        <h3 className="text-danger"><i className="fas fa-exclamation-triangle text-danger" /> Oops! Something went wrong.</h3>
                        <p>Seems like the server is not working properly. We will look into this issue as soon as possible.
          Meanwhile, you may <a href="../../index.html">return to login page</a>.</p>
                    </div>
                </div>

            </section>
        </div>






    )
}

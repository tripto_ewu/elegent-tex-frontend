import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formValid } from '../../services/FormValidationService';
import { connect } from 'react-redux';
import { register } from '../../store/actions/authActions';
import { clearErrors } from '../../store/actions/errorActions';
import Styles from './Signup.module.css'
import Input from '../helpers/input';
import Echo from "laravel-echo"
import Pusher from "pusher-js"

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);
const phoneRegex = RegExp(
    /^[0][1-9]\d{9}$|^[1-9]\d{11}$/
);
class Signup extends Component {

    state = {
        signupForm: {
            firstName: {
                elementType: 'input',
                label: 'First name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter First Name'
                },
                value: '',
                width: 'col-md-6',
                validationMsg: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            lastName: {
                elementType: 'input',
                label: 'Last name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Last Name'
                },
                value: '',
                width: 'col-md-6',
                validationMsg: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                label: 'Email',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Email Address'
                },
                value: '',
                width: 'col-md-6',
                validationMsg: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            phone: {
                elementType: 'input',
                label: 'Phone number',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Phone Number'
                },
                value: '',
                validation: {
                    required: true,
                    isPhone: true,
                },
                valid: false,
                width: 'col-md-6',
                validationMsg: '',
                touched: false
            },
            password: {
                elementType: 'input',
                label: 'Password',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter Password'
                },
                value: '',
                width: 'col-md-6',
                validationMsg: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
            confirmPassword: {
                elementType: 'input',
                label: 'Confirm Password',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Please Confirm Password'
                },
                value: '',
                width: 'col-md-6',
                validationMsg: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
        },
        formIsvalid: false,
        validationMsg: ''
    }

    checkFormValidity = (value, rules) => {
        let isValid = true;

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
            this.setState({ validationMsg: 'Please enter a valid input' })
        }
        if (rules.isPhone) {
            isValid = phoneRegex.test(value) && isValid;
            this.setState({ validationMsg: 'Please enter a valid phone number' })
        }
        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
            this.setState({ validationMsg: 'Minimum Length is 6 character' })
        }
        if (rules.isEmail) {
            isValid = emailRegex.test(value) && isValid;
            this.setState({ validationMsg: 'Please enter a valid Email' })
        }
        return isValid;
    }

    componentDidMount() {
        // window.Echo = new Echo({
        //     broadcaster: 'pusher',
        //     key: 'my_app_key',
        //     // host: 'http://localhost/elegent_tex/public',
        //     // port: 6001,
        //     disableStats: true,
        // });
        // console.log(window.Echo)

        // window.Echo.join(`chat`)
        //     .here((users) => {
        //         console.log(users)
        //     })
        //     .joining((user) => {
        //         console.log(user.name);
        //     })
        //     .leaving((user) => {
        //         console.log(user.name);
        //     });

    }

    handleChange = (event, inputIdentifier) => {

        const updatedSignupForm = { ...this.state.signupForm };
        const updatedFormElement = { ...updatedSignupForm[inputIdentifier] };
        console.log(updatedFormElement)
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkFormValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        console.log(updatedFormElement);
        updatedSignupForm[inputIdentifier] = updatedFormElement;
        let formIsvalid = true;
        for (let inputIdentifier in updatedSignupForm) {
            formIsvalid = updatedSignupForm[inputIdentifier].valid && formIsvalid;
        }
        this.setState({ signupForm: updatedSignupForm, formIsvalid: formIsvalid });
        console.log(this.state.formIsvalid)

    }
    register = () => {
        console.log(this.state.signupForm)
        console.log(this.state.formIsvalid)

        const formData = {};
        for (let formElementIdentifier in this.state.signupForm) {
            formData[formElementIdentifier] = this.state.signupForm[formElementIdentifier].value;
        }
        console.log(formData)

        if (this.state.formIsvalid) {
            this.props.register(formData);
        }
        if (this.state.errorMsg) {
            setTimeout(function () { this.props.clearErrors() }, 3000);
        }
        console.log(this.state)
    }
    componentDidUpdate(prevProps) {
        const { error, clearErrors, isAuthenticated } = this.props;

        if (isAuthenticated) {
            this.props.history.push('/');
        }
        if (error !== prevProps.error) {
            if (error.id === 'REGISTER_FAIL') {
                this.setState({ ...this.state, errorMsg: error.msg })
                setTimeout(function () { clearErrors() }, 3000);
            } else {
                this.setState({ ...this.state, errorMsg: null })
            }

        }
        console.log(error)
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.signupForm) {
            formElementsArray.push({
                id: key,
                config: this.state.signupForm[key]
            });
        }
        let form = (
            <form onSubmit={this.register}>
                <div className="row">
                    {formElementsArray.map(formElement => {
                        return <Input
                            key={formElement.id}
                            elementName={formElement.id}
                            width={formElement.config.width}
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            label={formElement.config.label}
                            value={formElement.config.value}
                            invalid={!formElement.config.valid}
                            touched={formElement.config.touched}
                            validationMsg={this.state.validationMsg}
                            changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                        />
                    })}
                </div>
                <div className="row">
                    <div className="col-8">
                        <p className="mb-0">
                            <Link to="/login">Already a user? Login</Link>
                        </p>
                    </div>
                    <div className="col-4">
                        <button type="button" className="btn btn-block btn-primary" onClick={this.register}>Sign Up</button>
                    </div>
                </div>
            </form>
        );

        return (
            <div className="register-page">

                <div className={Styles.registerBox} >
                    <div className="login-logo">
                        <a href="../../index2.html"><b>Care</b>Taker</a>
                    </div>
                    {/* /.login-logo */}
                    <div className="card">
                        <div className="card-body login-card-body">
                            <p className="login-box-msg">Sign Up as a new user</p>
                            {form}
                        </div>
                        {/* /.login-card-body */}
                    </div>
                </div>
            </div >
        )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.error
})

export default connect(mapStateToProps, { register, clearErrors })(Signup);
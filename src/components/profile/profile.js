
import React, { Component } from 'react'
import ContentHeader from '../extras/ContentHeader'
import { connect } from 'react-redux';
import UpdateProfile from './updateProfile';

class profile extends Component {

    state = {

        unmountUpdateProduct: true,

    }
    updateProfile = (product) => {
        this.setState({ unmountUpdateProduct: !this.state.unmountUpdateProduct })
    }
    handleUnmountUpdate = () => {
        this.setState({ unmountUpdateProduct: !this.state.unmountUpdateProduct })
    }

    componentDidMount() {

    }
    render() {

        const { first_name, last_name, email, is_admin, phone_no, image_link } = this.props.userData;
        console.log(process.env.REACT_APP_STORAGE + image_link)

        return (
            <div className="content-wrapper">
                <ContentHeader name="Profile" />
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="card card-info card-outline">
                                    <div className="card-body box-profile">
                                        <div className="text-center">
                                            {image_link ? <img className="profile-user-img img-fluid img-circle" src={process.env.REACT_APP_STORAGE + image_link} alt="/images/avatar.png" /> :
                                                <img className="profile-user-img img-fluid img-circle" src="/images/avatar.png" alt="user" />}
                                        </div>
                                        <h3 className="profile-username text-center">{first_name} {last_name}</h3>
                                        <p className="text-muted text-center">{is_admin === 0 ? 'Seller' : "Admin"}</p>
                                        <ul className="list-group list-group-unbordered mb-3">
                                            <li className="list-group-item">
                                                <span className="text-info"><b>Email</b></span><a className="float-right">{email}</a>
                                            </li>
                                            <li className="list-group-item">
                                                <span className="text-info"><b>Phone Number</b></span><a className="float-right">{`+88${phone_no}`}</a>
                                            </li>
                                            <li className="list-group-item">
                                                <span className="text-info"><b>Revenue</b></span><a className="float-right">13,287</a>
                                            </li>
                                        </ul>
                                        <button className="btn btn-info btn-block"
                                            onClick={this.updateProfile}><b>Update User Info</b></button>
                                    </div>
                                    {/* /.card-body */}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {!this.state.unmountUpdateProduct ?
                    <UpdateProfile
                        userData={this.props.userData}
                        unmountUpdateProduct={this.handleUnmountUpdate} /> : null}
            </div>
        )
    }
}


const mapStateToProps = state => ({
    userData: state.auth.user,
    token: state.auth.access_token,
})

export default connect(mapStateToProps)(profile);

import React, { Component } from 'react';
import Input from '../helpers/input';
import { toast } from 'react-toastify';
import axios from 'axios';
import { formValid } from '../helpers/formValidationChecker';

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

toast.configure();
const phoneRegex = RegExp(
    /^[0][1-9]\d{9}$|^[1-9]\d{11}$/
);


export default class UpdateProfile extends Component {

    state = {
        updateProfileForm: {
            firstName: {
                elementType: 'input',
                label: 'First name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter first name'
                },
                value: this.props.userData.first_name,
                validationMsg: '',
                width: 'col-md-6',
                touched: false
            },
            lastName: {
                elementType: 'input',
                label: 'Last name',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter last name'
                },
                value: this.props.userData.last_name,
                validationMsg: '',
                width: 'col-md-6',
                touched: false
            },
            email: {
                elementType: 'input',
                label: 'Email',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter email address'
                },
                value: this.props.userData.email,
                validationMsg: '',
                width: 'col-md-6',
                touched: false
            },
            phone: {
                elementType: 'input',
                label: 'Phone Number',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter phone number'
                },
                value: this.props.userData.phone_no,
                validationMsg: '',
                width: 'col-md-6',
                touched: false
            },
            image: {
                elementType: 'file',
                label: 'Upload Image',
                elementConfig: {
                    type: 'file',
                },
                file: '',
                validationMsg: '',
                width: 'col-md-6',
                touched: false
            },

        }
    }

    checkFormValidity = (value, identifier) => {
        let validationMsg = '';
        switch (identifier) {
            case 'firstName':
                validationMsg = value.length < 1 ? "Enter first name" : '';
                return validationMsg;
            case 'lastName':
                validationMsg = value.length < 1 ? "Enter last name" : '';
                return validationMsg;
            case 'email':
                validationMsg = emailRegex.test(value) ? '' : "Enter a valid Email";
                return validationMsg;
            case 'phone':
                validationMsg = !phoneRegex.test(value) ? "Enter valid phone number" : '';
                return validationMsg;
            case 'image':
                console.log(value)
                if (value.size > 350000) {
                    validationMsg = "Maximum size is 350kb"

                }
                else if (!value.name.match(/\.(jpg|jpeg|png|gif|JPG)$/)) {
                    validationMsg = "Please Enter a valid image"

                }
                return validationMsg;
            default:
                return '';
        }

    }

    handleChange = (event, inputIdentifier) => {
        //console.log(event.target.files[0])
        const updatedProfileForm = { ...this.state.updateProfileForm };
        const updatedFormElement = { ...updatedProfileForm[inputIdentifier] };
        if (updatedFormElement.elementType === 'file') {
            updatedFormElement.file = event.target.files[0];
            console.log(event.target.files[0])
            if (event.target.files[0]) {
                updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.file, inputIdentifier);
            } else {
                updatedFormElement.validationMsg = ''
            }

        } else {
            updatedFormElement.value = event.target.value;
            updatedFormElement.validationMsg = this.checkFormValidity(updatedFormElement.value, inputIdentifier);
        }
        updatedFormElement.touched = true;
        updatedProfileForm[inputIdentifier] = updatedFormElement;
        this.setState({ updateProfileForm: updatedProfileForm }, () => {
            console.log(this.state)
        });


    }

    uploadImage = () => {
        const id = this.props.userData.id;
        const imageData = new FormData();
        imageData.append('image', this.state.updateProfileForm.image.file, this.state.updateProfileForm.image.file.name);
        return axios.post(`${process.env.REACT_APP_API_KEY}/api/uploadProfileImage/${id}`, imageData).then((response) => {
            return response.data
        }).catch(error => {
            return null;
        });

    }

    updateProfile = async (event) => {
        event.preventDefault();
        let formData = {};
        const formError = {}

        for (let formElementIdentifier in this.state.updateProfileForm) {
            if (this.state.updateProfileForm[formElementIdentifier].elementType !== 'file') {
                formData[formElementIdentifier] = this.state.updateProfileForm[formElementIdentifier].value
            }
            formError[formElementIdentifier] = this.state.updateProfileForm[formElementIdentifier].validationMsg;
        }
        if (this.state.updateProfileForm.image.file && this.state.updateProfileForm.image.validationMsg === '') {
            const filePath = await this.uploadImage();
            formData = { ...formData, image: filePath };
        } else {
            formData = { ...formData, image: this.props.userData.image_link };
        }

        console.log(formError);
        const id = this.props.userData.id;
        if (formValid(formError, formData)) {
            axios.post(`${process.env.REACT_APP_API_KEY}/api/updateUserProfile/${id}`, formData).then(response => {
                // this.props.updateProduct(response.data, 'update');
                // toast.success("Product is updated !!!");
                console.log(response)
                window.$("#update-profile-modal").modal("hide");
                this.props.unmountUpdateProduct();
            }).catch(err => {

                toast.success("Product Update Error!!!");
            })
        }
        else {
            toast.error("Please fill all the required fields !!!");
        }

    }
    componentDidMount() {
        window.$('#update-profile-modal').modal('show');
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.updateProfileForm) {
            formElementsArray.push({
                id: key,
                config: this.state.updateProfileForm[key]
            });
        }
        let form = (
            <form onSubmit={this.updateProfile}>
                <div className="modal-body">
                    <div className="row">
                        {formElementsArray.map(formElement => {

                            return <Input
                                key={formElement.id}
                                elementName={formElement.id}
                                elementType={formElement.config.elementType}
                                label={formElement.config.label}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                width={formElement.config.width}
                                touched={formElement.config.touched}
                                validationMsg={formElement.config.validationMsg}
                                changeHandler={(event, inputIdentifier) => this.handleChange(event, formElement.id)}
                            />
                        })}
                    </div>
                </div>
                <div className="modal-footer justify-content-between">
                    <button type="button" className="btn btn-default btn-sm" data-dismiss="modal" onClick={this.props.unmountUpdateProduct}>Close</button>
                    <button type="submit" className="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        );
        return (
            <div className="modal fade" data-keyboard="false" data-backdrop="static" id="update-profile-modal">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Edit Color</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.unmountUpdateProduct}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        {form}
                    </div>
                </div>
            </div>

        )
    }
}

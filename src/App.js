import React, { useEffect } from 'react';
import MainLayout from './components/layout/MainLayout';
import Settings from './components/content/settings/Settings'
import Login from './components/authentication/Login';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadUser } from './store/actions/authActions';
import Logout from './components/authentication/Logout';
import Error from './components/extras/404';
import Signup from './components/authentication/Signup';
import Dashboard from './components/content/Dashboard';
import Products from './components/content/products/Products';
import Orders from './components/content/orders/Orders';
import CreateOrder from './components/content/orders/CreateOrder';
import Profile from './components/profile/profile'
import Echo from "laravel-echo"
import Pusher from "pusher-js"
import Checkout from './components/content/orders/Checkout';
import Invoice from './components/content/orders/Invoice';

function App(props) {

  useEffect(() => {
    if (props.auth.access_token) {
      props.loadUser();



    } else {
      console.log(props)
    }

    window.Echo = new Echo({
      broadcaster: 'pusher',
      key: 'myAppKey',
      wsHost: window.location.hostname,
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
      enabledTransports: ['ws', 'wss'],
      // auth: {
      //   headers: {
      //     //Authorization: `Bearer ${this.props.token}`,
      //     Accept: 'application/json',
      //   },
      // },
    });


  }, [props.auth.isAuthenticated]);

  return (
    <div className="wrapper">
      <BrowserRouter>
        <Switch>
          <Route path="/login" exact component={Login} />
          <Route path="/signup" exact component={Signup} />
          <Route path="/logout" exact component={Logout} />
          <Route path="/error" exact component={Error} />
        </Switch>
        <MainLayout>
          <Switch>
            {/* <Route path="/" exact component={Content} /> */}
            <Route path="/" exact component={Dashboard} />
            <Route path="/products" exact component={Products} />
            <Route path="/orders" exact component={Orders} />
            <Route path="/settings" exact component={Settings} />
            <Route path="/profile" exact component={Profile} />
            <Route path="/createNewOrder" exact component={CreateOrder} />
            <Route path="/createNewOrder/checkout" exact component={Checkout} />
            <Route path="/createNewOrder/invoice/:id" exact component={Invoice} />

            <Route path='*' exact={true} component={Error} />
          </Switch>
        </MainLayout>
      </BrowserRouter>
    </div>
  );
}
const mapStateToProps = state => ({
  auth: state.auth,
})

export default connect(mapStateToProps, { loadUser })(App)
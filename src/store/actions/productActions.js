import axios from 'axios';
import {
    PRODUCT_LOADED,
    PRODUCT_LOADING,
    PRODUCT_ERROR,
    PRODUCT_COLOR_LOADED,
    PRODUCT_COLOR_LOADING,
    PRODUCT_COLOR_ERROR,
    PRODUCT_TYPE_LOADED,
    PRODUCT_TYPE_LOADING,
    PRODUCT_TYPE_ERROR,

} from './types';
import { returnErrors } from './errorActions';

export const loadProduct = () => {
    //console.log("load User")
    return async (dispatch, getState) => {
        dispatch({ type: PRODUCT_LOADING });
        await axios.get(process.env.REACT_APP_API_KEY + '/api/getAllProducts').then(response => {
            dispatch({ type: PRODUCT_LOADED, payload: response.data });
            console.log(response.data)
        }).catch(err => {
            console.log(err)
            dispatch(returnErrors(err, err));
            dispatch({ type: PRODUCT_ERROR });
        })
    }
}
export const loadProductColor = () => {
    return async (dispatch, getState) => {
        dispatch({ type: PRODUCT_COLOR_LOADING });
        await axios.get(process.env.REACT_APP_API_KEY + '/api/getAllColors').then(response => {
            dispatch({ type: PRODUCT_COLOR_LOADED, payload: response.data });
        }).catch(err => {
            console.log(err)
            dispatch(returnErrors(err, err));
            dispatch({ type: PRODUCT_COLOR_ERROR });
        })
    }
}

export const loadProductTypes = () => {
    return async (dispatch, getState) => {
        dispatch({ type: PRODUCT_TYPE_LOADING });
        await axios.get(process.env.REACT_APP_API_KEY + '/api/getAllTypes').then(response => {
            dispatch({ type: PRODUCT_TYPE_LOADED, payload: response.data });
        }).catch(err => {
            console.log(err)
            dispatch(returnErrors(err, err));
            dispatch({ type: PRODUCT_TYPE_ERROR });
        })
    }
}
export const updateExistingColors = (element, operation) => {
    return (dispatch, getState) => {
        const elements = getState().products.productColors;
        const productColors = updateElements(elements, element, operation)
        dispatch({ type: 'UPDATE_PRODUCT_COLORS', payload: productColors });
    }


}
export const updateExistingProducts = (element, operation) => {
    return (dispatch, getState) => {
        const elements = getState().products.products;
        const products = updateElements(elements, element, operation)
        dispatch({ type: 'UPDATE_EXISTING_PRODUCTS', payload: products });
    }


}

function updateElements(elements, element, operation) {
    const updatedElements = [...elements];
    var foundIndex = updatedElements.findIndex(x => x.id === element.id);
    if (foundIndex !== -1) {
        operation === 'update' ? updatedElements[foundIndex] = element : updatedElements.splice(foundIndex, 1);
    } else {
        updatedElements.unshift(element)
    }
    return updatedElements;
}

import {
    ORDER_LOADED,
    ORDER_LOADING,
    ORDER_ERROR,
} from './types';
import axios from 'axios';
import { returnErrors } from './errorActions';

export const orderCart = (data) => {
    console.log(data)
    return (dispatch, getState) => {
        dispatch({ type: 'CREATE_ORDER', payload: data });
    }
}

export const loadAllOrders = (url, makePagination) => {
    return async (dispatch, getState) => {
        dispatch({ type: ORDER_LOADING });
        console.log(url)
        await axios.get(process.env.REACT_APP_API_KEY + url).then(response => {
            dispatch({ type: ORDER_LOADED, payload: response.data });
            makePagination(response.data)
            console.log(response.data)
        }).catch(err => {
            console.log(err)
            dispatch(returnErrors(err, err));
            dispatch({ type: ORDER_ERROR });
        })


    }
}

export const updateExistingOrders = (element, operation) => {
    return (dispatch, getState) => {
        console.log(getState());
        console.log(element.order);
        const elements = getState().orders.orders;
        const orders = updateElements(elements, element.order, operation)
        dispatch({ type: 'UPDATE_ORDERS', payload: orders });
    }
}

function updateElements(elements, element, operation) {
    const updatedData = { ...elements }
    const updatedElement = [...updatedData.data];
    console.log(updatedElement)
    var foundIndex = updatedElement.findIndex(x => x.id === element.id);
    if (foundIndex !== -1) {
        operation === 'update' ? updatedElement[foundIndex] = element : updatedElement.splice(foundIndex, 1);
    } else {
        updatedElement.unshift(element)
    }
    updatedData['data'] = updatedElement;
    return updatedData;
}
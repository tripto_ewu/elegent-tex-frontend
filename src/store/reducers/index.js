import { combineReducers } from 'redux';
import errorReducer from './errorReducer';
import authReducers from './authReducer';
import productReducers from './productReducer';
import orderReducer from './orderReducer';

export default combineReducers({
    error: errorReducer,
    auth: authReducers,
    products: productReducers,
    orders: orderReducer
});
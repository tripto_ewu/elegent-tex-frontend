import {
    PRODUCT_LOADED,
    PRODUCT_LOADING,
    PRODUCT_ERROR,
    PRODUCT_COLOR_ERROR,
    PRODUCT_COLOR_LOADING,
    PRODUCT_COLOR_LOADED,
    PRODUCT_TYPE_ERROR,
    PRODUCT_TYPE_LOADING,
    PRODUCT_TYPE_LOADED
} from "../actions/types";

const initialState = {
    isLoading: false,
    products: [],
    productColors: [],
    productTypes: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case PRODUCT_LOADING:
        case PRODUCT_TYPE_LOADING:
        case PRODUCT_COLOR_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case PRODUCT_LOADED:
            return {
                ...state,
                isLoading: false,
                products: action.payload
            };
        case PRODUCT_COLOR_LOADED:
            return {
                ...state,
                isLoading: false,
                productColors: action.payload
            };
        case PRODUCT_TYPE_LOADED:
            return {
                ...state,
                isLoading: false,
                productTypes: action.payload
            }
        case 'UPDATE_EXISTING_PRODUCTS':
            return {
                ...state,
                isLoading: false,
                products: action.payload
            };
        case 'UPDATE_PRODUCT_COLORS':
            return {
                ...state,
                isLoading: false,
                productColors: action.payload
            };

        case PRODUCT_ERROR:
        case PRODUCT_COLOR_ERROR:
        case PRODUCT_TYPE_ERROR:
            return {
                ...state,
                products: [],
                isLoading: false
            }
        default:
            return state;
    }
}
import {
    ORDER_LOADED,
    ORDER_LOADING,
    ORDER_ERROR,
} from "../actions/types";


const initialState = {
    orders: [],
    isLoading: false
}
export default function (state = initialState, action) {
    switch (action.type) {
        case ORDER_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case ORDER_LOADED:
            return {
                ...state,
                isLoading: false,
                orders: action.payload
            };

        case ORDER_ERROR:
            return {
                ...state,
                orders: [],
                isLoading: false
            }
        case 'UPDATE_ORDERS':
            console.log(action.payload)
            return {
                ...state,
                isLoading: false,
                orders: action.payload
            };
        default:
            return state;
    }
}